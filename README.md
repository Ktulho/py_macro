Scripts for [XVM](https://modxvm.com/) (py_macro)

### Official Support Forums

* [Сборник py_macro](https://kr.cm/f/t/39885/)
* [Прицел (без сведения) средствами XVM](https://kr.cm/f/t/37259/)
* [assistLog (py_macro)](https://kr.cm/f/t/47047/)
* [Отображение активных резервов в ангаре и в бою. (XVM)](https://kr.cm/f/t/41426/)
* [Статистические данные (py_macro)](https://kr.cm/f/t/38422/)
* [УГН и УВН средствами XVM](https://kr.cm/f/t/36508/)
* [DamageIndicator средствами XVM](https://kr.cm/f/t/36620/)
* [Статистика техники в ангаре. (XVM)](https://kr.cm/f/t/42167/)
* [BattleTimer средствами XVM.](https://kr.cm/f/t/36869/)
* [Цвета контура обводки техники и сообщений в логе смертей (XVM)](https://kr.cm/f/t/41514/)
* [Маркеры панели счета (py_macro)](https://kr.cm/f/t/38779/)
* [Активация, продление, обновление XVM из клиента.](https://kr.cm/f/t/42364/)
* [Часы и дата (py_macro)](https://kr.cm/f/t/35339/)
* [Шанс на победу (py_macro)](https://kr.cm/f/t/38417/)


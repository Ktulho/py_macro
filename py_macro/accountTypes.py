from gui.Scaleform.daapi.view.lobby.header.LobbyHeader import LobbyHeader
from gui.impl import backport
from gui.impl.gen import R

from xfw.events import overrideMethod
from xvm_main.python.logger import *


@overrideMethod(LobbyHeader, 'as_setPremiumParamsS')
def LobbyHeaderMeta_as_setPremiumParamsS(base, self, data):
    data['btnLabelShort'] = ''
    data['btnLabel'] = ''
    data['doLabel'] = ''
    if not data['isPremium']:
        data['isPremium'] = True
        data['premiumIcon'] = backport.image(R.images.gui.maps.icons.premacc.lobbyHeader.noPremium())
    return base(self, data)


log('[PY_MACRO] loaded accountTypes.py')

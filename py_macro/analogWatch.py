from datetime import datetime, time
from math import degrees, pi

DEGREES_PER_SECOND = 2 * pi / 60.0
DEGREES_PER_MINUTE = 2 * pi / 60.0
DEGREES_PER_HOUR = 2 * pi / (60.0 * 12)
TURN_TO_ZERO = -135


@xvm.export('xvm.secondHand', deterministic=False)
def xvm_formatDate():
    d = datetime.now()
    return degrees(d.second * DEGREES_PER_SECOND) + TURN_TO_ZERO


@xvm.export('xvm.minuteHand', deterministic=False)
def xvm_formatDate():
    d = datetime.now()
    return degrees(d.minute * DEGREES_PER_MINUTE) + TURN_TO_ZERO


@xvm.export('xvm.hourHand', deterministic=False)
def xvm_formatDate():
    d = datetime.now()
    hour = d.hour if d.hour < 12 else d.hour - 12
    minute = hour * 60 + d.minute
    return degrees(minute / DEGREES_PER_HOUR) + TURN_TO_ZERO

import BigWorld
from Vehicle import Vehicle
from Avatar import PlayerAvatar
from gui.Scaleform.daapi.view.battle.classic.team_bases_panel import TeamBasesPanel

from xfw.events import registerEvent
from xvm_main.python.logger import *
from xfw_actionscript.python import *

teamCaptured = None
resetTeamCapturedID = None


def resetTeamCaptured():
    global teamCaptured, resetTeamCapturedID
    teamCaptured = None
    resetTeamCapturedID = None
    as_event('ON_CAPTURED')


@registerEvent(Vehicle, '_Vehicle__onAppearanceReady')
def _Vehicle__onAppearanceReady(self, appearance):
    if self.isPlayerVehicle:
        global teamCaptured
        teamCaptured = None
        as_event('ON_CAPTURED')


@registerEvent(TeamBasesPanel, 'setTeamBaseCaptured')
def setTeamBaseCaptured(self, clientID, playerTeam):
    global teamCaptured, resetTeamCapturedID
    teamCaptured = 'ally' if playerTeam != (clientID & 63) else 'enemy'
    resetTeamCapturedID = BigWorld.callback(10, resetTeamCaptured)
    as_event('ON_CAPTURED')


@registerEvent(PlayerAvatar, '_PlayerAvatar__destroyGUI')
def PlayerAvatar__destroyGUI(self):
    if resetTeamCapturedID is not None:
        BigWorld.cancelCallback(resetTeamCapturedID)


@xvm.export('teamCaptured', deterministic=False)
def _teamCaptured():
    return teamCaptured

import math

import BigWorld
import constants
import gui.Scaleform.daapi.view.battle.shared.crosshair.plugins as plug
from Avatar import PlayerAvatar
from AvatarInputHandler import AvatarInputHandler
from AvatarInputHandler.gun_marker_ctrl import _CrosshairShotResults, _SHOT_RESULT
from DestructibleEntity import DestructibleEntity
from Vehicle import Vehicle
from Vehicle import Vehicle as VehicleEntity
from aih_constants import CTRL_MODE_NAME
from items.components.component_constants import MODERN_HE_PIERCING_POWER_REDUCTION_FACTOR_FOR_SHIELDS, MODERN_HE_DAMAGE_ABSORPTION_FACTOR

import xvm_battle.python.battle as battle
import xvm_main.python.config as config
from xfw.events import registerEvent, overrideClassMethod
from xfw_actionscript.python import *
from xvm_main.python.logger import *

NOT_PIERCED = 'not_pierced'
LITTLE_PIERCED = 'little_pierced'
GREAT_PIERCED = 'great_pierced'
NOT_TARGET = 'not_target'
DISPLAY_IN_MODES = [CTRL_MODE_NAME.ARCADE,
                    CTRL_MODE_NAME.ARTY,
                    CTRL_MODE_NAME.DUAL_GUN,
                    CTRL_MODE_NAME.SNIPER,
                    CTRL_MODE_NAME.STRATEGIC,
                    'flamethrower']

PIERCING_CHANCE_KEY = (NOT_TARGET, NOT_PIERCED, LITTLE_PIERCED, GREAT_PIERCED)
COLOR_PIERCING_CHANCE = {NOT_PIERCED: '#E82929',
                         LITTLE_PIERCED: '#E1C300',
                         GREAT_PIERCED: '#2ED12F',
                         NOT_TARGET: ''}

piercingActual = None
armorActual = None
piercingChance = None
shotResult = NOT_TARGET
hitAngle = None
normHitAngle = None
colorPiercingChance = COLOR_PIERCING_CHANCE
visible = True


@registerEvent(AvatarInputHandler, 'onControlModeChanged')
def AvatarInputHandler_onControlModeChanged(self, eMode, **args):
    global visible
    newVisible = eMode in DISPLAY_IN_MODES
    if newVisible != visible:
        visible = newVisible
        as_event('ON_CALC_ARMOR')


@registerEvent(plug.ShotResultIndicatorPlugin, '_ShotResultIndicatorPlugin__onGunMarkerStateChanged')
def onGunMarkerStateChanged(self, markerType, position, direction, collision):
    if config.get('sight/enabled', True) and battle.isBattleTypeSupported:
        if not self._ShotResultIndicatorPlugin__isEnabled:
            self._ShotResultIndicatorPlugin__shotResultResolver.getShotResult(position, collision, direction,
                                                                              excludeTeam=self._ShotResultIndicatorPlugin__playerTeam)


@overrideClassMethod(_CrosshairShotResults, 'getShotResult')
def _CrosshairShotResults_getShotResult(base, cls, hitPoint, collision, direction, excludeTeam=0, piercingMultiplier=1):
    global shotResult

    def update():
        global piercingActual, armorActual, hitAngle, normHitAngle, piercingChance
        if (armorActual is not None) or (piercingActual is not None) or (hitAngle  is not None):
            piercingActual = None
            armorActual = None
            piercingChance = None
            hitAngle = None
            normHitAngle = None
            as_event('ON_CALC_ARMOR')
            cls._CrosshairShotResults__sendDebugInfo([])

    if config.get('sight/enabled', True) and battle.isBattleTypeSupported:
        shotResult = PIERCING_CHANCE_KEY[_SHOT_RESULT.UNDEFINED]
        if collision is None:
            update()
            return _SHOT_RESULT.UNDEFINED
        entity = collision.entity
        if not isinstance(entity, (VehicleEntity, DestructibleEntity)):
            update()
            return _SHOT_RESULT.UNDEFINED
        if entity.health <= 0 or entity.publicInfo['team'] == excludeTeam:
            update()
            return _SHOT_RESULT.UNDEFINED
        player = BigWorld.player()
        if player is None:
            update()
            return _SHOT_RESULT.UNDEFINED
        vDesc = player.getVehicleDescriptor()
        shell = vDesc.shot.shell
        shellKind = shell.kind
        ppDesc = vDesc.shot.piercingPower
        maxDist = vDesc.shot.maxDistance
        dist = (hitPoint - player.getOwnVehiclePosition()).length
        fullPiercingPower = cls._computePiercingPowerAtDist(ppDesc, dist, maxDist, piercingMultiplier)
        collisionsDetails = cls._getAllCollisionDetails(hitPoint, direction, entity)
        if collisionsDetails is None:
            update()
            return _SHOT_RESULT.UNDEFINED
        minPP, maxPP = cls._computePiercingPowerRandomization(shell)
        if shellKind == constants.SHELL_TYPES.HIGH_EXPLOSIVE and shell.type.mechanics == constants.SHELL_MECHANICS_TYPE.MODERN:
            return cls._CrosshairShotResults__shotResultModernHE(collisionsDetails, fullPiercingPower, shell, minPP, maxPP, entity)
        else:
            return cls._CrosshairShotResults__shotResultDefault(collisionsDetails, fullPiercingPower, shell, minPP, maxPP, entity)
    else:
        return base(hitPoint, collision, direction, excludeTeam, piercingMultiplier)


@overrideClassMethod(_CrosshairShotResults, '_CrosshairShotResults__shotResultModernHE')
def _CrosshairShotResults__shotResultModernHE(base, cls, collisionsDetails, fullPiercingPower, shell, minPP, maxPP, entity):
    if not (config.get('sight/enabled', True) and battle.isBattleTypeSupported):
        return base(collisionsDetails, fullPiercingPower, shell, minPP, maxPP, entity)
    global piercingActual, armorActual, shotResult, hitAngle, normHitAngle, piercingChance
    old_piercingActual = piercingActual
    old_armorActual = armorActual
    old_hitAngle = hitAngle
    piercingActual = None
    armorActual = None
    piercingChance = None
    hitAngle = None
    normHitAngle = None
    shotResult = PIERCING_CHANCE_KEY[_SHOT_RESULT.UNDEFINED]
    result = _SHOT_RESULT.NOT_PIERCED
    ignoredMaterials = set()
    piercingPower = fullPiercingPower
    dispersion = round(piercingPower) * shell.piercingPowerRandomization
    minPiercingPower = round(round(piercingPower) - dispersion)
    maxPiercingPower = round(round(piercingPower) + dispersion)
    explosionDamageAbsorption = 0
    debugPiercingsList = []
    for cDetails in collisionsDetails:
        if not cls._CrosshairShotResults__isDestructibleComponent(entity, cDetails.compName):
            return result
        matInfo = cDetails.matInfo
        if matInfo is not None and (cDetails.compName, matInfo.kind) not in ignoredMaterials:
            hitAngleCos = cDetails.hitAngleCos if matInfo.useHitAngle else 1.0
            hitAngle = hitAngleCos
            normHitAngle = hitAngle
            piercingPercent = 1000.0
            penetrationArmor = 0
            if fullPiercingPower > 0.0:
                penetrationArmor = cls._computePenetrationArmor(shell, hitAngleCos, matInfo)
                normHitAngle = matInfo.armor / penetrationArmor if penetrationArmor != 0.0 else hitAngle
                piercingPercent = 100.0 + (penetrationArmor - piercingPower) / fullPiercingPower * 100.0
                piercingActual = int(piercingPower)
                armorActual = int(penetrationArmor)
                armorRatio = penetrationArmor / piercingPower - 1.0 if piercingPower > 0.0 else 1.0
                # piercingChance = 1.0 if armorRatio < -0.25 else 0.5 * math.erfc(8.485281374238576 * armorRatio) if armorRatio <= 0.25 else 0.0
                if armorRatio < -0.25:
                    piercingChance = 1.0
                elif armorRatio > 0.25:
                    piercingChance = 0.0
                else:
                    piercingChance = 0.5 * math.erfc(8.485281374238576 * armorRatio)
            if matInfo.vehicleDamageFactor:
                piercingPower -= penetrationArmor
                minPiercingPower = round(minPiercingPower - penetrationArmor)
                maxPiercingPower = round(maxPiercingPower - penetrationArmor)
                if piercingPercent <= minPP and explosionDamageAbsorption == 0:
                    result = _SHOT_RESULT.GREAT_PIERCED
                else:
                    result = _SHOT_RESULT.LITTLE_PIERCED
                cls._CrosshairShotResults__collectDebugPiercingData(debugPiercingsList, penetrationArmor, hitAngleCos, minPiercingPower, maxPiercingPower,
                                                                    piercingPercent, matInfo, result)
                cls._CrosshairShotResults__sendDebugInfo(debugPiercingsList, minPP, maxPP, fullPiercingPower)
                shotResult = PIERCING_CHANCE_KEY[result]
                return result
            if shell.type.shieldPenetration:
                shieldPenetration = penetrationArmor * MODERN_HE_PIERCING_POWER_REDUCTION_FACTOR_FOR_SHIELDS
                piercingPower -= shieldPenetration
                minPiercingPower = round(minPiercingPower - shieldPenetration)
                maxPiercingPower = round(maxPiercingPower - shieldPenetration)
                explosionDamageAbsorption += penetrationArmor * MODERN_HE_DAMAGE_ABSORPTION_FACTOR
            if piercingPercent > maxPP or not shell.type.shieldPenetration or explosionDamageAbsorption >= shell.type.maxDamage:
                cls._CrosshairShotResults__collectDebugPiercingData(debugPiercingsList, penetrationArmor, hitAngleCos, minPiercingPower, maxPiercingPower,
                                                                    piercingPercent, matInfo, _SHOT_RESULT.NOT_PIERCED)
                cls._CrosshairShotResults__sendDebugInfo(debugPiercingsList, minPP, maxPP, fullPiercingPower)
                shotResult = PIERCING_CHANCE_KEY[_SHOT_RESULT.NOT_PIERCED]
                return _SHOT_RESULT.NOT_PIERCED
            if matInfo.extra and piercingPercent <= maxPP:
                cls._CrosshairShotResults__collectDebugPiercingData(debugPiercingsList, penetrationArmor, hitAngleCos, minPiercingPower, maxPiercingPower,
                                                                    piercingPercent, matInfo, cls._CRIT_ONLY_SHOT_RESULT)
                result = cls._CRIT_ONLY_SHOT_RESULT
            if matInfo.collideOnceOnly:
                ignoredMaterials.add((cDetails.compName, matInfo.kind))
    cls._CrosshairShotResults__sendDebugInfo(debugPiercingsList, minPP, maxPP, fullPiercingPower)
    shotResult = PIERCING_CHANCE_KEY[result]
    if (old_armorActual != armorActual) or (old_piercingActual != piercingActual) or (old_hitAngle != hitAngle):
        as_event('ON_CALC_ARMOR')
    return result


@overrideClassMethod(_CrosshairShotResults, '_CrosshairShotResults__shotResultDefault')
def _CrosshairShotResults__shotResultDefault(base, cls, collisionsDetails, fullPiercingPower, shell, minPP, maxPP, entity):
    if not (config.get('sight/enabled', True) and battle.isBattleTypeSupported):
        return base(collisionsDetails, fullPiercingPower, shell, minPP, maxPP, entity)
    global piercingActual, armorActual, shotResult, hitAngle, normHitAngle, piercingChance
    old_piercingActual = piercingActual
    old_armorActual = armorActual
    old_hitAngle = hitAngle
    piercingActual = None
    armorActual = None
    piercingChance = None
    hitAngle = None
    normHitAngle = None
    shotResult = PIERCING_CHANCE_KEY[_SHOT_RESULT.UNDEFINED]
    result = _SHOT_RESULT.NOT_PIERCED
    isJet = False
    jetStartDist = None
    piercingPower = fullPiercingPower
    dispersion = round(piercingPower) * shell.piercingPowerRandomization
    minPiercingPower = round(round(piercingPower) - dispersion)
    maxPiercingPower = round(round(piercingPower) + dispersion)
    ignoredMaterials = set()
    debugPiercingsList = []
    for cDetails in collisionsDetails:
        if not cls._CrosshairShotResults__isDestructibleComponent(entity, cDetails.compName):
            break
        if isJet:
            jetDist = cDetails.dist - jetStartDist
            if jetDist > 0.0:
                lossByDist = 1.0 - jetDist * cls._SHELL_EXTRA_DATA[shell.kind].jetLossPPByDist
                piercingPower *= lossByDist
                minPiercingPower = round(minPiercingPower * lossByDist)
                maxPiercingPower = round(maxPiercingPower * lossByDist)
        if cDetails.matInfo is None:
            result = cls._CRIT_ONLY_SHOT_RESULT
        else:
            matInfo = cDetails.matInfo
            if (cDetails.compName, matInfo.kind) in ignoredMaterials:
                continue
            hitAngleCos = cDetails.hitAngleCos if matInfo.useHitAngle else 1.0
            hitAngle = hitAngleCos
            normHitAngle = hitAngle
            piercingPercent = 1000.0
            if not isJet and cls._shouldRicochet(shell, hitAngleCos, matInfo):
                normHitAngle = -1.0
                cls._CrosshairShotResults__collectDebugPiercingData(debugPiercingsList, None, hitAngleCos, minPiercingPower, maxPiercingPower, piercingPercent,
                                                                    matInfo,  _SHOT_RESULT.NOT_PIERCED)
                break
            penetrationArmor = 0
            if piercingPower > 0.0:
                penetrationArmor = cls._computePenetrationArmor(shell, hitAngleCos, matInfo)
                normHitAngle = matInfo.armor / penetrationArmor if penetrationArmor != 0.0 else hitAngle
                piercingPercent = 100.0 + (penetrationArmor - piercingPower) / fullPiercingPower * 100.0
                piercingActual = int(piercingPower)
                armorActual = int(penetrationArmor)
                armorRatio = penetrationArmor / piercingPower - 1.0
                # piercingChance = 1.0 if armorRatio < -0.25 else 0.5 * math.erfc(8.485281374238576 * armorRatio) if armorRatio <= 0.25 else 0.0
                if armorRatio < -0.25:
                    piercingChance = 1.0
                elif armorRatio > 0.25:
                    piercingChance = 0.0
                else:
                    piercingChance = 0.5 * math.erfc(8.485281374238576 * armorRatio)
                piercingPower -= penetrationArmor
                minPiercingPower = round(minPiercingPower - penetrationArmor)
                maxPiercingPower = round(maxPiercingPower - penetrationArmor)
            if matInfo.vehicleDamageFactor:
                if minPP < piercingPercent < maxPP:
                    result = _SHOT_RESULT.LITTLE_PIERCED
                elif piercingPercent <= minPP:
                    result = _SHOT_RESULT.GREAT_PIERCED
                cls._CrosshairShotResults__collectDebugPiercingData(debugPiercingsList, penetrationArmor, hitAngleCos, minPiercingPower, maxPiercingPower,
                                                                    piercingPercent, matInfo, result)
                break
            else:
                debugResut = _SHOT_RESULT.NOT_PIERCED
                if minPP < piercingPercent < maxPP:
                    debugResut = _SHOT_RESULT.LITTLE_PIERCED
                elif piercingPercent <= minPP:
                    debugResut = _SHOT_RESULT.GREAT_PIERCED
                if matInfo.extra:
                    if piercingPercent <= maxPP:
                        result = cls._CRIT_ONLY_SHOT_RESULT
                cls._CrosshairShotResults__collectDebugPiercingData(debugPiercingsList, penetrationArmor, hitAngleCos, minPiercingPower, maxPiercingPower,
                                                                    piercingPercent, matInfo, debugResut)
            if matInfo.collideOnceOnly:
                ignoredMaterials.add((cDetails.compName, matInfo.kind))
        if piercingPower <= 0.0:
            break
        if cls._SHELL_EXTRA_DATA[shell.kind].jetLossPPByDist > 0.0:
            isJet = True
            mInfo = cDetails.matInfo
            armor = mInfo.armor if mInfo is not None else 0.0
            jetStartDist = cDetails.dist + armor * 0.001
    cls._CrosshairShotResults__sendDebugInfo(debugPiercingsList, minPP, maxPP, fullPiercingPower)
    shotResult = PIERCING_CHANCE_KEY[result]
    if (old_armorActual != armorActual) or (old_piercingActual != piercingActual) or (old_hitAngle != hitAngle):
        as_event('ON_CALC_ARMOR')
    return result


@registerEvent(Vehicle, '_Vehicle__onAppearanceReady')
def _Vehicle__onAppearanceReady(self, appearance):
    global piercingActual, armorActual, shotResult, hitAngle, normHitAngle, colorPiercingChance, piercingChance, visible
    if self.isPlayerVehicle and config.get('sight/enabled', True) and battle.isBattleTypeSupported:
        piercingActual = None
        armorActual = None
        piercingChance = None
        shotResult = NOT_TARGET
        hitAngle = None
        normHitAngle = None
        visible = True
        colorPiercingChance = config.get('sight/c:piercingChance', COLOR_PIERCING_CHANCE)
        as_event('ON_CALC_ARMOR')


@registerEvent(PlayerAvatar, 'updateVehicleHealth')
def PlayerAvatar_updateVehicleHealth(self, vehicleID, health, deathReasonID, isCrewActive, isRespawn):
    if not (health > 0 and isCrewActive) and config.get('sight/enabled', True) and battle.isBattleTypeSupported:
        global piercingActual, armorActual, shotResult, hitAngle, normHitAngle, colorPiercingChance, piercingChance
        piercingActual = None
        armorActual = None
        piercingChance = None
        shotResult = NOT_TARGET
        hitAngle = None
        normHitAngle = None
        as_event('ON_CALC_ARMOR')


@xvm.export('sight.piercingActual', deterministic=False)
def sight_piercingActual():
    return piercingActual if visible else None


@xvm.export('sight.hitAngle', deterministic=False)
def sight_hitAngle():
    return math.degrees(math.acos(hitAngle)) if hitAngle is not None and visible else None


@xvm.export('sight.normHitAngle', deterministic=False)
def sight_normHitAngel():
    if visible:
        return normHitAngle if (normHitAngle is None) or (normHitAngle < 0) else math.degrees(math.acos(normHitAngle))


@xvm.export('sight.armorActual', deterministic=False)
def sight_piercingActual():
    return armorActual if visible else None


@xvm.export('sight.piercingChanceColor', deterministic=False)
def sight_piercingChanceColor():
    return colorPiercingChance.get(shotResult, colorPiercingChance.get(NOT_TARGET, None))


@xvm.export('sight.piercingChanceKey', deterministic=False)
def sight_piercingChanceKey():
    # if visible:
    #     return shotResult if shotResult is not None else NOT_TARGET
    return shotResult if visible else None


@xvm.export('sight.piercingChance', deterministic=False)
def sight_piercingChance(norm=None):
    global piercingChance
    if not visible:
        return None
    if piercingChance is not None:
        piercingChance = (piercingChance * 100 if norm is None else piercingChance * norm)
    return piercingChance

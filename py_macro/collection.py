import BigWorld
from Avatar import PlayerAvatar
from gui.battle_control.controllers.battle_field_ctrl import BattleFieldCtrl
from gui.Scaleform.daapi.view.battle.shared.messages.player_messages import PlayerMessages
from realm import CURRENT_REALM

from xfw.events import registerEvent
from xvm_main.python.logger import *

deadPlayer = {}
players = {}
_vehicles = {}


def update(vehicle):
    global players
    if not vehicle:
        return
    vehType = vehicle['vehicleType']
    if vehType is not None:
        gun = vehType.gun
        _miscAttrs = vehType.miscAttrs
        hardening = 'hardening' if vehicle['maxHealth'] >= int(vehType.maxHealth * 1.08) or _miscAttrs['healthFactor'] >= 1.08 else None
        reloadVehicle = gun.reloadTime * _miscAttrs.get('gunReloadTimeFactor', 1) / (1.0695 + 0.0043 * _miscAttrs.get('crewLevelIncrease', 0))
        circularVisionRadius = vehType.turret.circularVisionRadius * _miscAttrs.get('circularVisionRadiusFactor', 1)
        piercingPower = [shot.piercingPower[0] for shot in gun.shots]
        damage = [shot.shell.damage[0] for shot in gun.shots]
        #isAutoreload = (gun.autoreload.reloadTime[0] != 0.0)
        isClip = (gun.clip[0] > 1) #and not isAutoreload
        players[vehicle['fakeName']] = {'reloadVehicle': reloadVehicle,
                                        'circularVisionRadius': circularVisionRadius,
                                        'damage': damage[0],
                                        'piercingPower': piercingPower[0],
                                        'hardening': hardening,
                                        'isClip': 'clip' if isClip else None}


def updatePlayer(name):

    vehicles = BigWorld.player().arena.vehicles
    for vehicle in vehicles.itervalues():
        if vehicle['fakeName'] == name:
            update(vehicle)
            break


def updateHardening(vehicleID, newMaxHealth):
    global players
    vehicle = BigWorld.entity(vehicleID)
    if vehicle is not None:
        playerName = vehicle.publicInfo.name
        if playerName not in players:
            update(BigWorld.player().arena.vehicles.get(vehicleID, None))
        players[playerName]['hardening'] = 'hardening' if newMaxHealth >= int(vehicle.typeDescriptor.maxHealth * 1.08) else None


def PlayerMessages__onShowPlayerMessageByCode(self, code, postfix, targetID, attackerID, equipmentID, ignoreMessages):
    global deadPlayer, _vehicles
    if _vehicles is None or attackerID not in _vehicles:
        _vehicles = BigWorld.player().arena.vehicles
    target = _vehicles.get(targetID, None)
    attacker = _vehicles.get(attackerID, None)
    if attacker is not None:
        shortUserString = attacker['vehicleType'].type.shortUserString if attacker['vehicleType'] is not None else ''
        name = attacker['fakeName'] if 'name' in attacker else ''
        deadPlayer[target['fakeName']] = {'attackerName': name, 'attackerVehicle': shortUserString}
    else:
        deadPlayer[target['fakeName']] = {'attackerName': '', 'attackerVehicle': ''}


@registerEvent(PlayerAvatar, 'onEnterWorld')
def PlayerAvatar_onEnterWorld(self, prereqs):
    global deadPlayer, players, _vehicles
    deadPlayer = {}
    players = {}
    _vehicles = self.arena.vehicles
    for vehicle in _vehicles.itervalues():
        update(vehicle)


@registerEvent(BattleFieldCtrl, '_BattleFieldCtrl__setEnemyMaxHealth', True)
def _BattleFieldCtrl__setEnemyMaxHealth(self, vehicleID, currentMaxHealth, newMaxHealth):
    updateHardening(vehicleID, newMaxHealth)


@registerEvent(BattleFieldCtrl, '_BattleFieldCtrl__setAllyMaxHealth', True)
def _BattleFieldCtrl__setAllyMaxHealth(self, vehicleID, currentMaxHealth, newMaxHealth):
    updateHardening(vehicleID, newMaxHealth)


# @registerEvent(Vehicle, 'onHealthChanged', True)
# def onHealthChanged(self, newHealth, oldHealth, attackerID, attackReasonID):
#     global deadPlayer, _vehicles
#     if self.isAlive():
#         return
#     if _vehicles is None or attackerID not in _vehicles:
#         _vehicles = BigWorld.player().arena.vehicles
#     target = _vehicles.get(self.id, None)
#     attacker = _vehicles.get(attackerID, None)
#     if attacker is not None:
#         shortUserString = attacker['vehicleType'].type.shortUserString if attacker['vehicleType'] is not None else ''
#         name = attacker['fakeName'] if 'name' in attacker else ''
#         deadPlayer[target['fakeName']] = {'attackerName': name, 'attackerVehicle': shortUserString}
#     else:
#         deadPlayer[target['fakeName']] = {'attackerName': '', 'attackerVehicle': ''}


@xvm.export('killerName', deterministic=False)
def killerName(name):
    return deadPlayer[name].get('attackerName', None) if name in deadPlayer else None


@xvm.export('killerVehicle', deterministic=False)
def killerVehicle(name):
    return deadPlayer[name].get('attackerVehicle', None) if name in deadPlayer else None


@xvm.export('reloadVehicle')
def reloadVehicle(name):
    if name not in players:
        updatePlayer(name)
    return players[name].get('reloadVehicle', None) if name in players else None


@xvm.export('visionRadius')
def visionRadius(name):
    if name not in players:
        updatePlayer(name)
    return players[name].get('circularVisionRadius', None) if name in players else None


@xvm.export('piercingPower')
def piercingPower(name):
    if name not in players:
        updatePlayer(name)
    return players[name].get('piercingPower', None) if name in players else None


@xvm.export('shellDamage')
def shellDamage(name):
    if name not in players:
        updatePlayer(name)
    return players[name].get('damage', None) if name in players else None


@xvm.export('isHardening', deterministic=False)
def isHardening(name):
    if name not in players:
        updatePlayer(name)
    return players[name].get('hardening', None) if name in players else None


@xvm.export('isAmmoClip', deterministic=False)
def isClip(name):
    if name not in players:
        updatePlayer(name)
    return players[name].get('isClip', None) if name in players else None


if CURRENT_REALM == 'RU':
    registerEvent(PlayerMessages, '_PlayerMessages__onShowPlayerMessageByCode')(PlayerMessages__onShowPlayerMessageByCode)
else:
    registerEvent(PlayerMessages, '_onShowPlayerMessageByCode')(PlayerMessages__onShowPlayerMessageByCode)

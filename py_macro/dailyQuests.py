# -*- coding: utf-8 -*-

from gui.server_events.events_helpers import dailyQuestsSortFunc
from quest_cache_helpers import makeI18nString
from skeletons.gui.server_events import IEventsCache
from helpers import dependency
from gui.Scaleform.daapi.view.lobby.hangar.Hangar import Hangar

from xfw.events import registerEvent, overrideClassMethod
from xvm_main.python.logger import *

import xvm.total_Efficiency as te

questsDescription = {'easy': {},
                     'medium': {},
                     'hard': {},
                     'bonus': {}}

progressDataKey = {'#quests:dailyQuests/condition/winBattle_accum': 'battlesCount'}
progressDataValues = {'damaged': 0,
                      'kills': 0,
                      'damageDealt': 0,
                      'critsCount': 0,
                      'spotted': 0}


# 'damaged' = len(te.numberDamagedVehicles) if te.numberDamagedVehicles is not None else 0
# 'kills' = te.ribbonTypes['kill']
# 'damageDealt' = te.totalDamage
# 'critsCount' = te.ribbonTypes['crits']
# 'spotted' = te.ribbonTypes['spotted']
# 'xp' = None
# 'originalXP' = None


def readResultSection(result):
    description = key = value = descriptionKey = None
    for values in result:
        if isinstance(values, (list, tuple)):
            if 'description' in values:
                _, descriptionKey = values
                if 'key' in descriptionKey:
                    descriptionKey = descriptionKey['key']
                    description = makeI18nString(descriptionKey)
            elif 'key' in values:
                _, key = values
                if key:
                    key = key[0][1]
            elif isinstance(values[1], (list, tuple)) and 'value' in values[1][0]:
                _, value = values
                if value:
                    value = value[0][1]
    if value:
        if '%(value)' in description:
            description = description % {'value': value}
        else:
            value = None
    return {'description': description, 'descriptionKey': descriptionKey, 'key': key, 'value': value} if descriptionKey else {}


# @registerEvent(Hangar, '_Hangar__updateParams')
@registerEvent(Hangar, '_dispose')
def Hangar_dispose(self):
    global questsDescription
    eventsCache = dependency.instance(IEventsCache)
    quests = sorted(eventsCache.getDailyQuests().values(), key=dailyQuestsSortFunc)
    # log('ammoPanel = %s' % filter(lambda x: not x.startswith('_'), dir(self.ammoPanel)))
    for q in quests:
        log('getUserName = %s' % q.getUserName())
        log('getData = %s' % q.getData())
        log('getProgressData = %s' % q.getProgressData())
        conditions = q.getData()['conditions']
        log('conditions = %s' % conditions)
        progressData = q.getProgressData()
        questDescription = {}
        progressDataValue = 0
        for condition in conditions:
            if 'postBattle' in condition:
                _, postBattle = condition
                if postBattle:
                    name, result = postBattle[0]
                    # if 'results' == name:
                    questDescription = readResultSection(result)
            if 'common' in condition and len(condition[1]) > 1:
                common = condition[1]
                for name in common:
                    if 'bonusLimit' not in name:
                        if 'cumulative' in name:
                            _, cumulative = name
                            _, result = cumulative[0]
                        else:
                            _, result = name
                        questDescription = readResultSection(result)
            if questDescription:
                if None in progressData:
                    key = questDescription['key'] if questDescription['key'] else progressDataKey.get(questDescription['descriptionKey'], None)
                    progressDataValue = progressData[None].get(key, 0)
                break
        questsDescription[q.getLevel()].update({'description': questDescription.get('description', None),
                                                'key': questDescription.get('key', None),
                                                'value': questDescription.get('value', None),
                                                'isCompleted': q.isCompleted(),
                                                'progressData': progressDataValue})
        log('questsDescription[{}] = [description = {}, descriptionKey = {}, key = {}, value = {}, isCompleted = {}, progressData = {}]'.format(
            q.getLevel(), questDescription.get('description', None),  questDescription.get('descriptionKey', None),  questDescription.get('key', None),
            questDescription.get('value', None), q.isCompleted(), progressDataValue))

        # DailyQuest = ['accountReqs', 'bonusCond', 'eventsCache', 'getActiveTimeIntervals', 'getBonusCount', 'getBonuses', 'getChildren', 'getCompensation',
        #               'getData', 'getDescription', 'getFinishTime', 'getFinishTimeLeft', 'getFinishTimeRaw', 'getGroupID', 'getGroupType', 'getID', 'getIconID',
        #               'getLevel', 'getNearestActivityTimeLeft', 'getNotificationText', 'getParents', 'getParentsName', 'getPriority', 'getProgressData',
        #               'getProgressExpiryTime', 'getSortKey', 'getStartTime', 'getStartTimeLeft', 'getStartTimeRaw', 'getSuitableVehicles',
        #               'getTimeFromStartTillNow', 'getType', 'getUserName', 'getUserType', 'getWeekDays', 'hasPremIGRVehBonus', 'isAvailable', 'isBonus',
        #               'isCompensationPossible', 'isCompleted', 'isCompletedByGroup', 'isGuiDisabled', 'isHidden', 'isIGR', 'isOutOfDate', 'isShowedPostBattle',
        #               'isTokensOnSale', 'isTokensOnSaleDynamic', 'isValidVehicleCondition', 'itemsCache', 'linkedActions', 'lobbyContext', 'postBattleCond',
        #               'preBattleCond', 'setChildren', 'setGroupID', 'setParents', 'setParentsName', 'shouldBeShown', 'vehicleReqs'


def updateProgressData():
    global progressDataValues
    progressDataValues = {'damaged': len(te.numberDamagedVehicles) if te.numberDamagedVehicles is not None else 0,
                          'kills': te.ribbonTypes['kill'],
                          'damageDealt': te.totalDamage,
                          'critsCount': te.ribbonTypes['crits'],
                          'spotted': te.ribbonTypes['spotted']}


def getProgressData(quest):
    if quest.get('isCompleted', True):
        return None
    key = quest.get('key', None)
    progressData = quest.get('progressData', 0)
    value = None
    if key in progressDataValues:
        updateProgressData()
        value = progressDataValues[key]
    if progressData:
        value = progressData + value if value else progressData
    return value


def getQuestDescription(quest):
    return None if quest.get('isCompleted', True) else quest.get('description', '')


def getQuestValue(quest):
    return None if quest.get('isCompleted', True) else quest.get('value', '')


@xvm.export('dq.easyDescr', deterministic=False)
def dq_easyDescr():
    return getQuestDescription(questsDescription.get('easy', {}))


@xvm.export('dq.mediumDescr', deterministic=False)
def dq_mediumDescr():
    return getQuestDescription(questsDescription.get('medium', {}))


@xvm.export('dq.hardDescr', deterministic=False)
def dq_hardDescr():
    return getQuestDescription(questsDescription.get('hard', {}))


@xvm.export('dq.bonusDescr', deterministic=False)
def dq_bonusDescr():
    return getQuestDescription(questsDescription.get('bonus', {}))


@xvm.export('dq.easyProgress', deterministic=False)
def dq_easyProgress():
    return getProgressData(questsDescription.get('easy', {}))


@xvm.export('dq.mediumProgress', deterministic=False)
def dq_mediumProgress():
    return getProgressData(questsDescription.get('medium', {}))


@xvm.export('dq.hardProgress', deterministic=False)
def dq_hardProgress():
    return getProgressData(questsDescription.get('hard', {}))


@xvm.export('dq.bonusProgress', deterministic=False)
def dq_bonusProgress():
    return getProgressData(questsDescription.get('bonus', {}))


@xvm.export('dq.easyValue', deterministic=False)
def dq_easyValue():
    return getQuestValue(questsDescription.get('easy', {}))


@xvm.export('dq.mediumValue', deterministic=False)
def dq_mediumValue():
    return getQuestValue(questsDescription.get('medium', {}))


@xvm.export('dq.hardValue', deterministic=False)
def dq_hardValue():
    return getQuestValue(questsDescription.get('hard', {}))


@xvm.export('dq.bonusValue', deterministic=False)
def dq_bonusValue():
    return getQuestValue(questsDescription.get('bonus', {}))

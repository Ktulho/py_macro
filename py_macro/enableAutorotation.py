import BigWorld
from Vehicle import Vehicle
from gui.Scaleform.daapi.view.battle.shared.battle_timers import PreBattleTimer
from constants import VEHICLE_SETTING

from xfw.events import registerEvent
from xvm_main.python.logger import *

isWheeledTech = False


@registerEvent(Vehicle, '_Vehicle__onAppearanceReady')
def _Vehicle__onAppearanceReady(self, appearance):
    global isWheeledTech
    if self.isPlayerVehicle:
        isWheeledTech = self.isWheeledTech and self.typeDescriptor.hasSiegeMode
        if isWheeledTech:
            arenaPeriod = BigWorld.player().guiSessionProvider.shared.arenaPeriod
            startBattle = arenaPeriod.getPeriod() if arenaPeriod is not None else 0
            if startBattle >= 3:
                BigWorld.player().enableOwnVehicleAutorotation(True)


@registerEvent(PreBattleTimer, 'hideCountdown')
def hideCountdown(self, state, speed):
    if state == 3 and isWheeledTech:
        cell = BigWorld.player().cell
        if hasattr(cell, 'vehicle_changeSetting'):
            cell.vehicle_changeSetting(VEHICLE_SETTING.SIEGE_MODE_ENABLED, True)

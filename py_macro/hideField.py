from xvm_main.python.logger import *
from xfw_actionscript.python import *


highlight = None


def fildOver(data):
    global highlight
    highlight = 'over'
    as_event('ON_HIGHLIGHT')


def fildOut(data):
    global highlight
    highlight = None
    as_event('ON_HIGHLIGHT')


as_callback('fild_Over', fildOver)
as_callback('fild_Out', fildOut)


@xvm.export('highlight', deterministic=False)
def highlight():
    return highlight

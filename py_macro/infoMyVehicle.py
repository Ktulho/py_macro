import BattleReplay
import items.utils as items_utils
from CurrentVehicle import _CurrentVehicle
from Vehicle import Vehicle
from gui.shared.items_parameters import functions
from vehicle_outfit.outfit import Outfit
from vehicle_systems.camouflages import getOutfitComponent

from xfw.events import registerEvent
from xvm_main.python.logger import *

WHEEL = 'wheel'
SIEGE = 'siege'
AUTO_SIEGE = 'autosiege'
DUAL = 'dual'
AUTO = 'auto'
TURBO = 'turbo'
CLIP = 'clip'

DEFAULT_VALUE_FEATURE = [WHEEL, SIEGE, AUTO_SIEGE, DUAL, AUTO, TURBO, CLIP]

featureVehicle = {v: False for v in DEFAULT_VALUE_FEATURE}
indexFeature = -1
invisibilityMoving = None


@registerEvent(_CurrentVehicle, 'destroy', True)
def destroy(self):
    global invisibilityMoving
    if self.item:
        factors = functions.getVehicleFactors(self.item)
        invisibilityMoving, _ = items_utils.getClientInvisibility(self.item.descriptor, self.item, factors.get('camouflage', 1), factors)


@registerEvent(Vehicle, '_Vehicle__onAppearanceReady')
def _Vehicle__onAppearanceReady(self, appearance):
    global featureVehicle, indexFeature, invisibilityMoving
    if self.isPlayerVehicle:
        typeDescriptor = self.typeDescriptor
        featureVehicle[WHEEL] = self.isWheeledTech
        featureVehicle[AUTO_SIEGE] = typeDescriptor.hasAutoSiegeMode
        featureVehicle[DUAL] = typeDescriptor.isDualgunVehicle
        featureVehicle[AUTO] = (typeDescriptor.gun.autoreload.reloadTime[0] != 0.0)
        featureVehicle[CLIP] = (typeDescriptor.gun.clip[0] > 1) and not featureVehicle[AUTO]
        featureVehicle[TURBO] = typeDescriptor.hasTurboshaftEngine
        featureVehicle[SIEGE] = not (featureVehicle[AUTO_SIEGE] or featureVehicle[WHEEL] or featureVehicle[DUAL] or featureVehicle[TURBO]) and typeDescriptor.hasSiegeMode
        # features = [i for i, v in enumerate(DEFAULT_VALUE_FEATURE) if featureVehicle.get(v, False)]
        # indexFeature = features[0] if features else -1
        indexFeature = -1
        for i, v in enumerate(DEFAULT_VALUE_FEATURE):
            if featureVehicle.get(v, False):
                indexFeature = i
                break
        if BattleReplay.g_replayCtrl.isPlaying or invisibilityMoving is None:
            outfit = Outfit(component=getOutfitComponent(self.publicInfo['outfit'], typeDescriptor), vehicleCD=typeDescriptor.makeCompactDescr())
            camouflageBonus = typeDescriptor.type.invisibilityDeltas['camouflageBonus'] if outfit.id else 0.0
            vehicleFactor = typeDescriptor.miscAttrs['invisibilityFactor']
            invMoving, invStill = typeDescriptor.type.invisibility
            moving = invMoving * 0.57 * vehicleFactor + camouflageBonus
            invisibilityMoving = moving + typeDescriptor.miscAttrs['invisibilityAdditiveTerm']


@xvm.export('isWheeledTech', deterministic=False)
def export_isWheeledTech():
    return WHEEL if featureVehicle[WHEEL] else None


@xvm.export('hasSiegeMode', deterministic=False)
def export_hasSiegeMode():
    return SIEGE if featureVehicle[SIEGE] else None


@xvm.export('hasAutoSiegeMode', deterministic=False)
def export_hasAutoSiegeMode():
    return AUTO_SIEGE if featureVehicle[AUTO_SIEGE] else None


@xvm.export('isDualGun', deterministic=False)
def export_isDualGunVehicle():
    return DUAL if featureVehicle[DUAL] else None


@xvm.export('isAutoReload', deterministic=False)
def export_isAutoReload():
    return AUTO if featureVehicle[AUTO] else None


@xvm.export('isClip', deterministic=False)
def export_isClip():
    return CLIP if featureVehicle[CLIP] else None


@xvm.export('hasTurboshaftEngine', deterministic=False)
def export_hasTurboshaftEngine():
    return TURBO if featureVehicle[TURBO] else None


@xvm.export('featureVehicle', deterministic=False)
def export_isDualGunVehicle(*args):
    if indexFeature < 0:
        return None
    return args[indexFeature] if len(args) > indexFeature else DEFAULT_VALUE_FEATURE[indexFeature]


@xvm.export('invisibilityMove', deterministic=False)
def invisibilityMove():
    return invisibilityMoving

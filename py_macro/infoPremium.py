import math
from constants import PREMIUM_TYPE
from gui.Scaleform.daapi.view.lobby.header.LobbyHeader import LobbyHeader
from helpers import time_utils, i18n

from xfw.events import registerEvent
from xvm_main.python.logger import *
from xfw_actionscript.python import as_event

timeLeft = None
timeMetric = None
isPremiumAccount = False
premiumType = None
days = 0
hours = 0
minutes = 0


@registerEvent(LobbyHeader, '_LobbyHeader__setAccountsAttrs')
def _LobbyHeader__setAccountsAttrs(self):
    global timeMetric, timeLeft, isPremiumAccount, premiumType, days, hours, minutes
    isPremiumAccount = self.itemsCache.items.stats.isPremium
    premiumExpiryTime = self.itemsCache.items.stats.activePremiumExpiryTime
    if isPremiumAccount:
        deltaInSeconds = time_utils.getTimeDeltaFromNow(time_utils.makeLocalServerTime(premiumExpiryTime))
        if deltaInSeconds > time_utils.ONE_DAY:
            timeLeft = int(math.ceil(float(deltaInSeconds) / time_utils.ONE_DAY))
            timeMetric = i18n.makeString('#menu:header/account/premium/days')
        else:
            timeLeft = int(math.ceil(float(deltaInSeconds) / time_utils.ONE_HOUR))
            timeMetric = i18n.makeString('#menu:header/account/premium/hours')
        days = deltaInSeconds / time_utils.ONE_DAY
        deltaInSeconds_h = deltaInSeconds - days * time_utils.ONE_DAY
        hours = deltaInSeconds_h / time_utils.ONE_HOUR
        t_minutes = deltaInSeconds_h - hours * time_utils.ONE_HOUR
        minutes = int(math.ceil(t_minutes / 60.0))
        premiumType = 'plus' if self.itemsCache.items.stats.isActivePremium(PREMIUM_TYPE.PLUS) else 'basic'
    else:
        timeLeft = None
        timeMetric = None
        premiumType = None
    as_event('ON_INFO_PREMIUM')


@xvm.export('prem.timeLeft', deterministic=False)
def prem_timeLeft():
    return timeLeft


@xvm.export('prem.timeMetric', deterministic=False)
def prem_timeMetric():
    return timeMetric


@xvm.export('prem.premType', deterministic=False)
def prem_premType():
    return premiumType


@xvm.export('prem.timeLeftDays', deterministic=False)
def prem_timeLeftDays():
    return days if isPremiumAccount else None


@xvm.export('prem.timeLeftHours', deterministic=False)
def prem_timeLeftHours():
    return hours if isPremiumAccount else None


@xvm.export('prem.timeLeftMinutes', deterministic=False)
def prem_timeLeftMinutes():
    return minutes if isPremiumAccount else None

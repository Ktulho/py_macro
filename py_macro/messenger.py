from xfw.events import overrideMethod
import messenger.gui.Scaleform.view.battle.messenger_view as messenger_view

@overrideMethod(messenger_view, '_makeSettingsVO')
def _makeSettingsVO(base, arenaVisitor):
    makeSettingsVO = base(arenaVisitor)
    makeSettingsVO['numberOfMessagesInHistory'] = 6
    return makeSettingsVO

from gui.Scaleform.daapi.view.meta.MinimapMeta import MinimapMeta
from xfw.events import registerEvent
from xvm_main.python.logger import *
from xfw_actionscript.python import *


MAP_SIZES = [232, 282, 332, 412, 512, 632]
sizeMap = 232

@registerEvent(MinimapMeta, 'as_setSizeS')
def MinimapMeta_as_setSizeS(self, size):
    global sizeMap
    sizeMap = MAP_SIZES[int(size)]
    as_event('ON_MAP_RESIZE')


@xvm.export('sizeMap', deterministic=False)
def size_Map():
    return sizeMap

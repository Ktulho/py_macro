from Vehicle import Vehicle
from gui.Scaleform.daapi.view.battle.shared.damage_panel import DamagePanel
from realm import CURRENT_REALM

from xfw.events import registerEvent
from xfw_actionscript.python import *
from xvm_main.python.logger import *

health = 0
maxHealth = 0
dmg = None


def DamagePanel__updateMaxHealth_Lesta(self):
    global maxHealth, health
    maxHealth = self._maxHealth
    as_event('ON_MY_HP')


def DamagePanel__updateMaxHealth_WG(self):
    global maxHealth, health
    maxHealth = self._DamagePanel__maxHealth
    as_event('ON_MY_HP')


@registerEvent(Vehicle, 'onHealthChanged')
def onHealthChanged(self, newHealth, oldHealth, attackerID, attackReasonID):
    if self.isPlayerVehicle:
        global health, dmg
        health = max(0, newHealth)
        dmg = oldHealth - health
        as_event('ON_MY_HP')


@registerEvent(Vehicle, '_Vehicle__onAppearanceReady')
def _Vehicle__onAppearanceReady(self, appearance):
    global health, dmg
    if self.isPlayerVehicle:
        health = self.health
        dmg = None
        as_event('ON_MY_HP')


@xvm.export('my_hp.health', deterministic=False)
def my_hp_health(norm=None):
    if health is None:
        return None
    if maxHealth != 0:
        return health if norm is None else int(norm * health // maxHealth)
    else:
        return health if norm is None else norm


@xvm.export('my_hp.maxHealth', deterministic=False)
def my_hp_maxHealth():
    return maxHealth


@xvm.export('my_hp.dmg', deterministic=False)
def my_hp_dmg():
    return dmg


if CURRENT_REALM == 'RU':
    registerEvent(DamagePanel, '_updateMaxHealth')(DamagePanel__updateMaxHealth_Lesta)
else:
    registerEvent(DamagePanel, '_DamagePanel__updateMaxHealth')(DamagePanel__updateMaxHealth_WG)
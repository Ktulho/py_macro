import BigWorld
from gui.Scaleform.daapi.view.battle.shared.messages.fading_messages import FadingMessages
from constants import ARENA_GUI_TYPE
from Vehicle import Vehicle

from xfw.events import registerEvent, overrideMethod


isFrontLine = False


# @registerEvent(Vehicle, '_Vehicle__onAppearanceReady')
# def _Vehicle__onAppearanceReady(self, appearance):
#     global isFrontLine
#     if self.isPlayerVehicle:
#         isFrontLine = BigWorld.player().arenaGuiType == ARENA_GUI_TYPE.EPIC_BATTLE


@overrideMethod(FadingMessages, 'showMessage')
def FadingMessages_showMessage(base, self, key, args=None, extra=None, postfix=''):
    # if not isFrontLine:
    #     return base(self, key, args, extra, postfix)
    pass

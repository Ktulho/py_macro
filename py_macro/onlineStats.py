from helpers import i18n
from gui import makeHtmlString
from gui.impl import backport
from gui.impl.gen import R
from gui.game_control.ServerStats import STATS_TYPE
from gui.Scaleform.locale.TOOLTIPS import TOOLTIPS
from gui.Scaleform.locale.MENU import MENU
from gui.shared.utils.functions import makeTooltip
from gui.Scaleform.daapi.view.lobby.header.LobbyHeader import LobbyHeader

from xvm_main.python.logger import *
import xvm_main.python.config as config
from xfw.events import overrideMethod

from xvm.parser_addon import parser_addon


clusterStatsHTML = ''
regionStatsHTML = ''
autoReloadConfig = False
isShow = True


def readConfig():
    global clusterStatsHTML, regionStatsHTML, autoReloadConfig, isShow
    isShow = config.get('hangar/serverInfo/enabled', True)
    autoReloadConfig = config.get('autoReloadConfig', False)
    clusterStatsHTML = config.get('hangar/serverInfo/clusterStats', '')
    regionStatsHTML = config.get('hangar/serverInfo/regionStats', '')


readConfig()


@overrideMethod(LobbyHeader, '_LobbyHeader__onStatsReceived')
def LobbyHeader__onStatsReceived(base, self):
    global clusterStatsHTML, regionStatsHTML
    if autoReloadConfig:
        readConfig()
    if not isShow:
        return None
    clusterUsers, regionUsers, tooltipType = self.serverStats.getStats()
    if tooltipType == STATS_TYPE.UNAVAILABLE:
        tooltip = TOOLTIPS.HEADER_INFO_PLAYERS_UNAVAILABLE
        clusterUsers = regionUsers = ''
    elif tooltipType == STATS_TYPE.CLUSTER:
        tooltip = TOOLTIPS.HEADER_INFO_PLAYERS_ONLINE_REGION
    else:
        tooltip = TOOLTIPS.HEADER_INFO_PLAYERS_ONLINE_FULL
    macros = {'clusterUsers': clusterUsers,
              'serverName': self.connectionMgr.serverUserNameShort,
              'regionUsers': regionUsers,
              'total': i18n.makeString(MENU.ONLINECOUNTER_TOTAL)}
    if clusterStatsHTML:
        clusterStats = parser_addon(clusterStatsHTML, macros)
    else:
        clusterStats = makeHtmlString('html_templates:lobby', 'onlineCounter', {'key': self.connectionMgr.serverUserNameShort,
                                                                                'delimiter': backport.text(R.strings.common.common.colon()),
                                                                                'value': clusterUsers})
    if tooltipType == STATS_TYPE.FULL:
        if regionStatsHTML:
            regionStats = parser_addon(regionStatsHTML, macros)
        else:
            regionStats = makeHtmlString('html_templates:lobby', 'onlineCounter', {'key': backport.text(R.strings.menu.onlineCounter.total()),
                                                                                   'delimiter': backport.text(R.strings.common.common.colon()),
                                                                                   'value': regionUsers})
    else:
        regionStats = ''
    body = i18n.makeString('{}/body'.format(tooltip), servername=self.connectionMgr.serverUserName)
    header = '{}/header'.format(tooltip)
    isAvailable = tooltipType != STATS_TYPE.UNAVAILABLE
    self.as_updateOnlineCounterS(clusterStats, regionStats, makeTooltip(header, body), isAvailable)


import BigWorld
from adisp import process
from helpers import dependency
from skeletons.gui.game_control import IBrowserController

from xfw import *
from xvm_main.python.logger import *
from xfw_actionscript.python import *

browserCtrl = dependency.instance(IBrowserController)
selectField = None
isOpen = False
isClick = False

@process
def load(url):
    browserSize = (int(BigWorld.screenWidth() * 0.66), int(BigWorld.screenHeight() * 0.66))
    yield browserCtrl.load(url=url, title='', browserID=None, isAsync=False, browserSize=browserSize, showCloseBtn=True, showWaiting=True)


def openBrowser_Down(data):
    global isClick
    if data['buttonIdx'] == 0:
        isClick = True
        as_event('ON_OPEN_BROWSER')


def openBrowser_Over(data):
    global selectField
    selectField = 'highlight'
    as_event('ON_OPEN_BROWSER')


def openBrowser_Out(data):
    global selectField
    selectField = None
    as_event('ON_OPEN_BROWSER')


as_callback('openBrowser_Out', openBrowser_Out)
as_callback('openBrowser_Over', openBrowser_Over)
as_callback('openBrowser_Down', openBrowser_Down)


@xvm.export('ob.highlight', deterministic=False)
def ob_highlight():
    return selectField


@xvm.export('ob.address', deterministic=False)
def ob_address(url):
    global isClick
    if isClick:
        load(url)
        isClick = False

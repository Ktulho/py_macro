from Avatar import PlayerAvatar
from AvatarInputHandler import AvatarInputHandler
from BigWorld import cancelCallback, time, callback, entity
from Vehicle import Vehicle
from aih_constants import CTRL_MODE_NAME
from gui.battle_control import avatar_getter
from gui.Scaleform.daapi.view.battle.shared.crosshair.plugins import AmmoPlugin
from gui.Scaleform.daapi.view.battle.shared.damage_panel import DamagePanel
from gui.Scaleform.daapi.view.meta.CrosshairPanelContainerMeta import CrosshairPanelContainerMeta
from gui.Scaleform.daapi.view.meta.DualGunPanelMeta import DualGunPanelMeta
from gui.battle_control.controllers.consumables.ammo_ctrl import AmmoReplayPlayer, AmmoController

import xvm_battle.python.battle as battle
import xvm_main.python.config as config
from xfw.events import registerEvent, overrideMethod
from xfw_actionscript.python import *
from xvm_main.python.logger import *

leftTime = None
leftTimeShot = None
reloadTime = None
reloadTimeClip = None
reloadTimeShell = None
endReloadTime = 0.0
endAutoReloadTime = None
endReloadTimeShot = None
isAutoReload = False
isDualGun = False
autoReloadTime = None
autoReloadTimes = []
autoReloadLeftTime = None
reloadTimesClip = []
quantityInClipShells = 0
quantityInClipShellsMax = 0
isAlive = True
reloadTimerCallbackID = None
autoReloadTimerCallbackID = None
reloadShotTimerCallbackID = None
tankmenAndDevicesReload = []
increasedReload = []
gunsState = [False, False]
isPreparingSalvo = False
isGunBlocked = False
visible = True

DISPLAY_IN_MODES = [CTRL_MODE_NAME.ARCADE,
                    CTRL_MODE_NAME.ARTY,
                    CTRL_MODE_NAME.DUAL_GUN,
                    CTRL_MODE_NAME.SNIPER,
                    CTRL_MODE_NAME.STRATEGIC,
                    'flamethrower']


def resetCallback(callbackID):
    if callbackID is not None:
        cancelCallback(callbackID)
    return None


def reloadTimer():
    global leftTime, reloadTimerCallbackID
    # log('reloadTimer')
    if leftTime is None:
        return
    leftTime = max(0.0, endReloadTime - time())
    # log('reloadTimer   leftTime = %s' % leftTime)
    if leftTime > 0.0:
        reloadTimerCallbackID = callback(0.1, reloadTimer)
    else:
        reloadTimerCallbackID = None
    if not isAutoReload:
        # log('reloadTimer')
        as_event('ON_RELOAD')


def reloading(duration, baseTime, startTime=0.0):
    global leftTime, reloadTime, endReloadTime, reloadTimeClip, reloadTimerCallbackID
    reloadTimerCallbackID = resetCallback(reloadTimerCallbackID)

    if duration > 0.0:
        leftTime = duration if leftTime != 0.0 else (duration - startTime)
        endReloadTime = time() + leftTime
        reloadTimerCallbackID = callback(0.1, reloadTimer)
    else:
        leftTime = 0.0
    if (baseTime != 0.0) and ((reloadTimeClip is None) or (abs(reloadTimeClip - baseTime) > 0.1)):
        reloadTime = baseTime
    # log('reloadTime = %s, duration = %s  baseTime = %s' % (reloadTime, duration, baseTime))
    if not isAutoReload:
        # log('reloading')
        as_event('ON_RELOAD')


def autoReloadTimer():
    global autoReloadLeftTime, autoReloadTimerCallbackID, leftTime
    if autoReloadLeftTime is None:
        return
    autoReloadLeftTime = max(0.0, endAutoReloadTime - time())
    leftTime = max(0.0, endReloadTime - time())
    if (autoReloadLeftTime > 0.0) or (leftTime > 0.0):
        autoReloadTimerCallbackID = callback(0.1, autoReloadTimer)
    else:
        autoReloadTimerCallbackID = None
    # log('autoReloadTimer')
    as_event('ON_RELOAD')


def autoReloading(duration, baseTime):
    global autoReloadLeftTime, autoReloadTime, endAutoReloadTime, autoReloadTimerCallbackID, endReloadTime, leftTime, reloadTime
    _currentReloadTimeInClip = autoReloadTimes[quantityInClipShells]
    factor = (baseTime / _currentReloadTimeInClip) if _currentReloadTimeInClip != 0 else 0
    autoReloadTime = factor * reloadTimesClip[0]
    # log('autoReloadTime = %s, factor = %s, reloadTimesClip[0] = %s duration = %s' % (autoReloadTime, factor, reloadTimesClip[0], duration))
    autoReloadTimerCallbackID = resetCallback(autoReloadTimerCallbackID)
    if duration > 0.0:
        autoReloadLeftTime = duration + factor * reloadTimesClip[quantityInClipShells + 1]
        leftTime = duration
        currentTime = time()
        endAutoReloadTime = currentTime + autoReloadLeftTime
        endReloadTime = currentTime + duration
        autoReloadTimerCallbackID = callback(0.1, autoReloadTimer)
    else:
        leftTime = autoReloadLeftTime = 0.0
    reloadTime = baseTime
    # log('autoReloading')
    # log('autoReloading: [%s] autoReloadTime = %s, factor = %s, autoReloadLeftTime = %s, reloadTime = %s' % (quantityInClipShells, autoReloadTime, factor, autoReloadLeftTime, reloadTime))
    as_event('ON_RELOAD')


def reloadShotTimer():
    global leftTimeShot, reloadShotTimerCallbackID
    if leftTimeShot is None:
        return
    leftTimeShot = max(0.0, endReloadTimeShot - time() - 0.1)
    if leftTimeShot > 0.0:
        reloadShotTimerCallbackID = callback(0.1, reloadShotTimer)
    else:
        reloadShotTimerCallbackID = None
    # log('reloadShotTimer')
    # as_event('ON_RELOAD')


def reloadingShot(duration):
    global leftTimeShot, reloadShotTimerCallbackID, endReloadTimeShot
    reloadShotTimerCallbackID = resetCallback(reloadShotTimerCallbackID)
    if duration > 0.0:
        leftTimeShot = duration - 0.1
        endReloadTimeShot = time() + duration
        reloadShotTimerCallbackID = callback(0.1, reloadShotTimer)
    else:
        leftTimeShot = 0.0
    # log('reloadingShot')
    as_event('ON_RELOAD')


def set_reloadTimeShell(ctrl):
    global reloadTimeShell
    quickShellChangeTime = ctrl.getQuickShellChangeTime()
    # log('quickShellChangeTime = %s' % quickShellChangeTime)
    if 0.1 < quickShellChangeTime != reloadTimeShell:
        reloadTimeShell = quickShellChangeTime
        as_event('ON_RELOAD')


def set_autoReload(reload):
    global isAutoReload, autoReloadTimes, reloadTimesClip
    isAutoReload = (reload[0] != 0.0)
    if isAutoReload:
        autoReloadTimes = list(reload)
        reloadTimesClip = []
        prev = 0
        for t in autoReloadTimes:
            reloadTimesClip.append(t + prev)
            prev += t
        reloadTimesClip.reverse()
        reloadTimesClip.append(0.0)
        reloadTimesClip.append(0.0)
        autoReloadTimes.reverse()
        autoReloadTimes.append(autoReloadTimes[len(autoReloadTimes) - 1])
    else:
        autoReloadTimes = None


@registerEvent(AmmoPlugin, '_AmmoPlugin__setupGuiSettings')
def __setupGuiSettings(self, gunSettings):
    if gunSettings.autoReload is not None:
        set_autoReload(gunSettings.autoReload.reloadTime)


@registerEvent(AmmoPlugin, '_AmmoPlugin__onGunAutoReloadTimeSet')
def __onGunAutoReloadTimeSet(self, state, stunned):
    if config.get('sight/enabled', True) and not isDualGun and battle.isBattleTypeSupported:
        # autoReloading(state.getActualValue(), round(state.getBaseValue(), 1))
        if self._AmmoPlugin__guiSettings.hasAutoReload:
            autoReloading(state.getActualValue(), state.getBaseValue())
        #     reloadingShot(state.getTimeLeft())


@registerEvent(AmmoPlugin, '_AmmoPlugin__onGunReloadTimeSet')
def reloading__onGunReloadTimeSet(self, _, state, skipAutoLoader):
    if config.get('sight/enabled', True) and not isDualGun and battle.isBattleTypeSupported:
        if self._AmmoPlugin__guiSettings.hasAutoReload:
            reloadingShot(state.getTimeLeft())
        else:
            reloading(state.getActualValue(), round(state.getBaseValue(), 1), state.getTimePassed())


@registerEvent(AmmoPlugin, '_AmmoPlugin__onQuickShellChangerUpdated')
def __onShellChangeTimeUpdated(self, isActive, time):
    global reloadTimeShell
    if config.get('sight/enabled', True) and battle.isBattleTypeSupported and reloadTimeShell != time:
        reloadTimeShell = time
        as_event('ON_RELOAD')


@registerEvent(PlayerAvatar, 'updateVehicleQuickShellChanger')
def updateVehicleQuickShellChanger(self, vehicleID, isActive):
    if config.get('sight/enabled', True) and battle.isBattleTypeSupported:
        set_reloadTimeShell(self.guiSessionProvider.shared.ammo)


@registerEvent(CrosshairPanelContainerMeta, 'as_setAmmoStockS')
def reloading_as_setAmmoStockS(self, quantity, quantityInClip,  *args, **kwargs):
    if config.get('sight/enabled', True) and battle.isBattleTypeSupported and not isDualGun:
        global quantityInClipShells
        quantityInClipShells = max(quantityInClip, 0) if quantityInClipShellsMax > 1 else None


@registerEvent(PlayerAvatar, 'updateVehicleGunReloadTime')
def updateVehicleGunReloadTime(self, vehicleID, timeLeft, baseTime,  *args, **kwargs):
    # log('updateVehicleGunReloadTime')
    if config.get('sight/enabled', True) and battle.isBattleTypeSupported:
        if reloadTimeShell is not None:
            set_reloadTimeShell(self.guiSessionProvider.shared.ammo)

        if not isDualGun and reloadTime is None:
            # log('updateVehicleGunReloadTime    vehicleID = %s, timeLeft = %s, baseTime = %s' % (vehicleID, timeLeft, baseTime))
            if isAutoReload:
                # log('updateVehicleGunReloadTime =    leftTeme = %s  baseTime = %s' % (timeLeft, baseTime))
                autoReloading(timeLeft, baseTime)
            else:
                reloading(timeLeft, baseTime)


@registerEvent(AmmoController, 'setGunReloadTime')
def reloading_setGunReloadTime(self, timeLeft, baseTime,  *args, **kwargs):
    if config.get('sight/enabled', True) and not isAutoReload and not isDualGun and battle.isBattleTypeSupported and avatar_getter.isVehicleAlive(None):
        # log('+++setGunReloadTime =    leftTeme = %s  baseTime = %s' % (timeLeft, baseTime))
        reloading(timeLeft, baseTime)


@registerEvent(DamagePanel, '_updateStun')
def reloading_updateStun(self, stunInfo):
    global increasedReload
    if config.get('sight/enabled', True) and battle.isBattleTypeSupported:
        stunDuration = stunInfo.duration
        if (stunDuration > 0) and ('stun' not in increasedReload):
            increasedReload.append('stun')
            as_event('ON_RELOAD')
        elif (stunDuration <= 0) and ('stun' in increasedReload):
            increasedReload.remove('stun')
            as_event('ON_RELOAD')
        # log('_updateStun increasedReload = %s' % increasedReload)


@registerEvent(DamagePanel, '_updateDeviceState')
def reloading_updateDeviceState(self, value):
    global increasedReload
    if config.get('sight/enabled', True) and battle.isBattleTypeSupported:
        role = value[0][:-1] if value[0] in ('gunner1', 'gunner2', 'radioman1', 'radioman2', 'loader1', 'loader2') else value[0]
        if role in tankmenAndDevicesReload:
            if value[1] == 'normal' and role in increasedReload:
                increasedReload.remove(role)
                as_event('ON_RELOAD')
            elif value[1] in ['critical', 'destroyed']:
                increasedReload.append(role)
                as_event('ON_RELOAD')
        # log('_updateDeviceState increasedReload = %s' % increasedReload)
        # log('_updateDeviceState role = %s    state = %s' % (value[0], value[1]))


@registerEvent(DualGunPanelMeta, 'as_updateTotalTimeS')
def as_updateTotalTimeS(self, value):
    global autoReloadTime
    # log('as_updateTotalTimeS   value = %s' % value)
    if config.get('sight/enabled', True) and battle.isBattleTypeSupported and isDualGun:
        value = round(value / 1000.0, 2)
        if autoReloadTime != value:
            autoReloadTime = value
            as_event('ON_RELOAD')


@registerEvent(DualGunPanelMeta, 'as_setGunStateS')
def as_setGunStateS(self, gunId, state, timeLeft, totalTime):
    global reloadTime, reloadShotTimerCallbackID, leftTimeShot
    # log('as_setGunStateS | gunId = %s, state = %s, timeLeft = %s, totalTime = %s' % (gunId, state, timeLeft, totalTime))
    if config.get('sight/enabled', True) and battle.isBattleTypeSupported:
        if reloadTime is None:
            reloadTime = round(totalTime / 1000.0, 2)
        if state == 2:
            reloadShotTimerCallbackID = resetCallback(reloadShotTimerCallbackID)
            leftTimeShot = 0.0
            reloading(round(timeLeft / 1000.0, 2), round(totalTime / 1000.0, 2))
        gunsState[gunId] = (state == 3)


@registerEvent(PlayerAvatar, 'updateDualGunState')
def updateDualGunState(self, vehicleID, activeGun, gunStates, cooldownTimes):
    global reloadTime, reloadShotTimerCallbackID, leftTimeShot, isGunBlocked, autoReloadTime
    # log('updateDualGunState | vehicleID = %s, activeGun = %s, gunStates = %s, cooldownTimes = %s' % (vehicleID, activeGun, gunStates, cooldownTimes))
    if config.get('sight/enabled', True) and battle.isBattleTypeSupported:
        update = False
        if reloadTime is None:
            reloadTime = cooldownTimes[activeGun].baseTime
            update = True
        if autoReloadTime is None:
            autoReloadTime = (cooldownTimes[0].baseTime + cooldownTimes[1].baseTime)
            update = True
        if update:
            as_event('ON_RELOAD')


@registerEvent(DualGunPanelMeta, 'as_updateActiveGunS')
def as_updateActiveGunS(self, activeGunId, timeLeft, totalTime):
    global isGunBlocked
    # log('as_updateActiveGunS | activeGunId = %s, timeLeft = %s, totalTime = %s' % (activeGunId, timeLeft, totalTime))
    if config.get('sight/enabled', True) and battle.isBattleTypeSupported:
        if (gunsState[0] + gunsState[1]) == 1:
            if timeLeft > 0:
                isGunBlocked = True
                reloadingShot(round(timeLeft / 1000.0, 2))
            elif isGunBlocked:
                isGunBlocked = False
                as_event('ON_RELOAD')


@registerEvent(DualGunPanelMeta, 'as_startChargingS')
def as_startChargingS(self, timeLeft, totalTime):
    global isPreparingSalvo
    # log('as_startChargingS timeLeft = %s, totalTime = %s' % (timeLeft, totalTime))
    if timeLeft > 0 and (gunsState[0] + gunsState[1]) == 2:
        isPreparingSalvo = True
        reloadingShot(round(timeLeft / 1000.0, 2))


@registerEvent(DualGunPanelMeta, 'as_cancelChargeS')
def as_cancelChargeS(self):
    global reloadShotTimerCallbackID, isPreparingSalvo, leftTimeShot
    # log('as_cancelChargeS')
    isPreparingSalvo = False
    leftTimeShot = 0.0
    reloadShotTimerCallbackID = resetCallback(reloadShotTimerCallbackID)
    as_event('ON_RELOAD')


@registerEvent(DualGunPanelMeta, 'as_setCooldownS')
def as_setCooldownS(self, timeLeft):
    global isPreparingSalvo, reloadTime
    # log('as_setCooldownS  timeLeft = %s' % timeLeft)
    if timeLeft > 0 and (gunsState[0] + gunsState[1]) == 0:
        if reloadTime is None:
            reloadTime = 0.0
        isPreparingSalvo = False
        reloadingShot(round(timeLeft / 1000.0 - reloadTime, 2))


@registerEvent(Vehicle, '_Vehicle__onAppearanceReady')
def _Vehicle__onAppearanceReady(self, appearance):
    global reloadTimeClip, increasedReload, gunsState, visible, isAlive, isPreparingSalvo
    global quantityInClipShellsMax, quantityInClipShells, autoReloadTime, tankmenAndDevicesReload, isDualGun, isGunBlocked
    if self.isPlayerVehicle and config.get('sight/enabled', True) and battle.isBattleTypeSupported:
        # set_reloadTimeShell(self.guiSessionProvider.shared.ammo)
        isAlive = True
        gunsState = [False, False]
        isPreparingSalvo = False
        isGunBlocked = False
        # log('self.quickShellChangerIsActive = %s' % self.quickShellChangerIsActive)
        # log('self._Vehicle__onAppearanceReady')
        gun = self.typeDescriptor.gun
        tankmenAndDevicesReload = [role[0] for role in self.typeDescriptor.type.crewRoles if 'loader' in role]
        tankmenAndDevicesReload.append('ammoBay')
        increasedReload = []
        isDualGun = self.typeDescriptor.isDualgunVehicle
        quantityInClipShells = 0
        quantityInClipShellsMax = 2 if isDualGun else gun.clip[0]
        set_autoReload(gun.autoreload.reloadTime)
        reloadTimeClip = gun.clip[1] if gun.clip[0] > 1 else None
        # for i, v in enumerate(gun.clip):
        #     log('gun.clip[%s] = %s' % (i, v))
        visible = True
        as_event('ON_RELOAD')


@registerEvent(AvatarInputHandler, 'onControlModeChanged')
def AvatarInputHandler_onControlModeChanged(self, eMode, **args):
    global visible
    # log('onControlModeChanged    eMode = %s' % eMode)
    newVisible = eMode in DISPLAY_IN_MODES
    if newVisible != visible:
        visible = newVisible
        as_event('ON_RELOAD')


def hide():
    global leftTime, reloadTime, endReloadTime, reloadTimeClip, autoReloadLeftTime, autoReloadTime, isAlive, leftTimeShot, isGunBlocked
    global increasedReload, reloadTimerCallbackID, autoReloadTimerCallbackID, reloadShotTimerCallbackID, isPreparingSalvo, reloadTimeShell
    isAlive = False
    leftTime = None
    leftTimeShot = None
    reloadTime = None
    reloadTimeClip = None
    endReloadTime = 0.0
    autoReloadLeftTime = None
    autoReloadTime = None
    increasedReload = []
    isPreparingSalvo = False
    isGunBlocked = False
    reloadTimeShell = None
    reloadTimerCallbackID = resetCallback(reloadTimerCallbackID)
    autoReloadTimerCallbackID = resetCallback(autoReloadTimerCallbackID)
    reloadShotTimerCallbackID = resetCallback(reloadShotTimerCallbackID)


@registerEvent(PlayerAvatar, '_PlayerAvatar__destroyGUI')
def PlayerAvatar__destroyGUI(self):
    if config.get('sight/enabled', True) and battle.isBattleTypeSupported:
        hide()


@registerEvent(PlayerAvatar, 'updateVehicleHealth')
def PlayerAvatar_updateVehicleHealth(self, vehicleID, health, deathReasonID, isCrewActive, isRespawn):
    if not (health > 0 and isCrewActive) and config.get('sight/enabled', True) and battle.isBattleTypeSupported:
        hide()
        as_event('ON_RELOAD')


@xvm.export('sight.leftTime', deterministic=False)
def sight_leftTime(norm=None):
    if not (visible and isAlive):
        return None
    if norm is None:
        return leftTime
    elif reloadTime is not None:
        if reloadTime == 0.0:
            return 0
        elif leftTime is not None:
            return int((reloadTime - leftTime) * norm // reloadTime)
        else:
            return None
    else:
        return None


@xvm.export('sight.reloadPercent', deterministic=False)
def sight_reloadPercent():
    if (reloadTime is not None) and visible and isAlive:
        if reloadTime == 0.0:
            return 0
        elif leftTime is not None:
            return int((reloadTime - leftTime) * 100 // reloadTime)
        else:
            return None
    return None


@xvm.export('sight.reloadTime', deterministic=False)
def sight_reloadTime():
    # log('reloadTime = %s' % reloadTime)
    return reloadTime if visible and isAlive else None


@xvm.export('sight.intuition', deterministic=False)
def sight_reloadTimeShell():
    return reloadTimeShell if visible and isAlive else None


@xvm.export('sight.reloadTimeClip', deterministic=False)
def sight_reloadTimeClip():
    return reloadTimeClip if visible and isAlive else None


@xvm.export('sight.aLeftTime', deterministic=False)
def sight_aLeftTime(norm=None):
    if not (visible and isAlive):
        return None
    if isDualGun and (reloadTime is not None) and (leftTime is not None):
        # log('(gunsState[0] = %s gunsState[1]) = %s leftTime = %s reloadTime = %s' % (gunsState[0], gunsState[1], leftTime, reloadTime))
        _leftTime = leftTime if (gunsState[0] + gunsState[1]) > 0 else leftTime + (autoReloadTime - reloadTime)
        if norm is None:
            return _leftTime
        elif autoReloadTime is not None:
            return 0 if autoReloadTime == 0.0 else int((autoReloadTime - _leftTime) * norm // autoReloadTime)
    elif autoReloadTime is not None:
        if norm is None:
            return autoReloadLeftTime
        else:
            if autoReloadTime is None:
                return None
            return 0 if autoReloadTime == 0.0 else int((autoReloadTime - autoReloadLeftTime) * norm // autoReloadTime)
    return None


@xvm.export('sight.leftTimeShot', deterministic=False)
def sight_leftTimeShot():
    if not isAlive or not visible:
        return None
    if isDualGun:
        numberLoadedGuns = gunsState[0] + gunsState[1]
        if numberLoadedGuns == 0:
            return leftTime if leftTime > 0.0 else leftTimeShot
        elif numberLoadedGuns == 1 or numberLoadedGuns == 2:
            return leftTimeShot
        else:
            return 0.0
    return leftTimeShot


@xvm.export('sight.aReloadTime', deterministic=False)
def sight_aReloadTime():
    return autoReloadTime if visible and isAlive else None


@xvm.export('sight.isIncreasedReload', deterministic=False)
def sight_isIncreasedReload():
    return '#FF0000' if increasedReload else None


@xvm.export('sight.isPreparingSalvo', deterministic=False)
def sight_preparingSalvo():
    return 'prepare' if isPreparingSalvo and visible and isAlive else None


@xvm.export('sight.isGunBlocked', deterministic=False)
def sight_isGunBlocked():
    return 'blocked' if isGunBlocked and visible and isAlive else None

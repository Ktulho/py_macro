import Keys
import game
from adisp import process, async
from helpers import dependency
from skeletons.gui.game_control import IBrowserController
from notification.NotificationListView import NotificationListView

from xfw.events import registerEvent, overrideMethod
from xvm_main.python.logger import *


ADDRESS_XVM_RU = 'https://modxvm.com/#wot-main'
ADDRESS_XVM_EN = 'https://modxvm.com/en/#wot-main'
ADDRESS_WARGAMING = 'https://wargaming.net/id/signin/'


browserCtrl = dependency.instance(IBrowserController)
browser = None
addressXVM = ''
isSiteXVM = True


@process
def load(url, title, browserID=None):
    global browser
    browserId = yield browserCtrl.load(url=url, title=None, browserID=browserID, isAsync=False, browserSize=(1200, 650), showCloseBtn=True, showWaiting=True)
    browser = browserCtrl.getBrowser(browserId)


@overrideMethod(NotificationListView, 'onClickAction')
def NotificationListView_onClickAction(base, self, typeID, entityID, action):
    global addressXVM
    # log('action = %s' % action)
    if action == ADDRESS_XVM_RU:
        addressXVM = ADDRESS_XVM_RU
        load(addressXVM, '\xc2\xa9')
    elif action == ADDRESS_XVM_EN:
        addressXVM = ADDRESS_XVM_EN
        load(addressXVM, '\xc2\xa9')
    else:
        base(self, typeID, entityID, action)


@registerEvent(game, 'handleKeyEvent')
def handleKeyEvent(event):
    global isSiteXVM
    isDown, key, mods, isRepeat = game.convertKeyEvent(event)
    if key == Keys.KEY_F7 and isDown and browser is not None and browser.hasBrowser:
        if isSiteXVM:
            browser.doNavigate(ADDRESS_WARGAMING)
        else:
            browser.doNavigate(addressXVM)
        isSiteXVM = not isSiteXVM


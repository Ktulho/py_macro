from BigWorld import callback, cancelCallback
from Avatar import PlayerAvatar
from Account import PlayerAccount
from Vehicle import Vehicle
from gui.Scaleform.daapi.view.lobby.hangar.Hangar import Hangar

import xvm_main.python.config as config
from xfw_actionscript.python import *
from xvm_main.python.logger import *
from xfw.events import registerEvent


hangarTimers = {}
battleTimers = {}


class Timer(object):

    def __init__(self, period, name):
        self.__period = period
        self.__name = name
        self.__id = None
        self.__timer()

    def __del__(self):
        self.stop()

    def __timer(self):
        if self.__period > 0 and self.__name:
            self.__id = callback(self.__period, self.__timer)
            as_event(self.__name)

    def stop(self):
        if self.__id is not None:
            cancelCallback(self.__id)
            self.__id = None

    def updatePeriod(self, period):
        self.stop()
        self.__period = period
        self.__timer()


@registerEvent(Vehicle, '_Vehicle__onAppearanceReady')
def _Vehicle__onAppearanceReady(self, appearance):
    global battleTimers
    if self.isPlayerVehicle:
        cfgTimers = config.get('events/battleTimers', dict())
        for timer in cfgTimers:
            name = timer.get('name', '')
            period = timer.get('period', 0)
            if name and name in battleTimers and period > 0:
                if name not in hangarTimers:
                    battleTimers[name] = Timer(period, name)
                else:
                    battleTimers[name].updatePeriod(period)


@registerEvent(PlayerAvatar, '_PlayerAvatar__destroyGUI')
def PlayerAvatar__destroyGUI(self):
    global battleTimers
    for value in battleTimers.itervalues():
        value.stop()


@registerEvent(PlayerAccount, 'onArenaCreated')
def onArenaCreated(self):
    global hangarTimers
    for value in hangarTimers.itervalues():
        value.stop()


@registerEvent(Hangar, '_populate')
def Hangar_populate(self):
    cfgTimers = config.get('events/hangarTimers', dict())
    for timer in cfgTimers:
        name = timer.get('name', '')
        period = timer.get('period', 0)
        if name and period > 0:
            if name not in hangarTimers:
                hangarTimers[name] = Timer(period, name)
            else:
                hangarTimers[name].updatePeriod(period)

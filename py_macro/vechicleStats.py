from CurrentVehicle import g_currentVehicle

from xvm_main.python.logger import *
from xfw_actionscript.python import *
from xfw.events import registerEvent


currentVehicle = {'Clan': None,
                  'Company': None,
                  'EpicRandom': None,
                  'Fallout': None,
                  'FortBattles': None,
                  'FortSorties': None,
                  'GlobalMap': None,
                  'Random': None,
                  'Ranked': None,
                  'Rated7x7': None,
                  'Team7x7': None,
                  'Total': None,
                  'Minimize': None}

events = {'Clan': 'ON_CLAN',
          'Company': 'ON_COMPANY',
          'EpicRandom': 'ON_EPIC_RANDOM',
          'Fallout': 'ON_FALLOUT',
          'FortBattles': 'ON_FORT_BATTLES',
          'FortSorties': 'ON_FORT_SORTIES',
          'GlobalMap': 'ON_GLOBAL_MAP',
          'Random': 'ON_RANDOM',
          'Ranked': 'ON_RANKED',
          'Rated7x7': 'ON_RATED7X7',
          'Team7x7': 'ON_TEAM7X7',
          'Total': 'ON_TOTAL',
          'Minimize': 'ON_MINIMIZE'}

visibleStats = 'Total'
selectField = None
deselectField = None
prevVisibleStats = None


def updateFilds_Down(newVisibleStats):
    global visibleStats
    _prevVisibleStats = visibleStats
    visibleStats = newVisibleStats
    as_event(events[_prevVisibleStats])
    as_event(events[visibleStats])


def clan_Down(data):
    if (data['buttonIdx'] == 0) and (visibleStats != 'Clan'):
        updateFilds_Down('Clan')


def company_Down(data):
    if (data['buttonIdx'] == 0) and (visibleStats != 'Company'):
        updateFilds_Down('Company')


def epicRandomn_Down(data):
    if (data['buttonIdx'] == 0) and (visibleStats != 'EpicRandom'):
        updateFilds_Down('EpicRandom')


def fallout_Down(data):
    if (data['buttonIdx'] == 0) and (visibleStats != 'Fallout'):
        updateFilds_Down('Fallout')


def fortBattles_Down(data):
    if (data['buttonIdx'] == 0) and (visibleStats != 'FortBattles'):
        updateFilds_Down('FortBattles')


def fortSorties_Down(data):
    if (data['buttonIdx'] == 0) and (visibleStats != 'FortSorties'):
        updateFilds_Down('FortSorties')


def globalMap_Down(data):
    if (data['buttonIdx'] == 0) and (visibleStats != 'GlobalMap'):
        updateFilds_Down('GlobalMap')


def random_Down(data):
    if (data['buttonIdx'] == 0) and (visibleStats != 'Random'):
        updateFilds_Down('Random')


def ranked_Down(data):
    if (data['buttonIdx'] == 0) and (visibleStats != 'Ranked'):
        updateFilds_Down('Ranked')


def rated7x7_Down(data):
    if (data['buttonIdx'] == 0) and (visibleStats != 'Rated7x7'):
        updateFilds_Down('Rated7x7')


def team7x7_Down(data):
    if (data['buttonIdx'] == 0) and (visibleStats != 'Team7x7'):
        updateFilds_Down('Team7x7')


def total_Down(data):
    if (data['buttonIdx'] == 0) and (visibleStats != 'Total'):
        updateFilds_Down('Total')


def minimize_Down(data):
    global visibleStats, prevVisibleStats
    if data['buttonIdx'] == 0:
        if visibleStats == 'Minimize':
            # visibleStats = prevVisibleStats
            # as_event('ON_MINIMIZE')
            # as_event(events[visibleStats])
            updateFilds_Down(prevVisibleStats)
        else:
            prevVisibleStats = visibleStats
            # visibleStats = 'Minimize'
            updateFilds_Down('Minimize')


as_callback('clan_Down', clan_Down)
as_callback('company_Down', company_Down)
as_callback('epicRandomn_Down', epicRandomn_Down)
as_callback('fallout_Down', fallout_Down)
as_callback('fortBattles_Down', fortBattles_Down)
as_callback('fortSorties_Down', fortSorties_Down)
as_callback('globalMap_Down', globalMap_Down)
as_callback('random_Down', random_Down)
as_callback('ranked_Down', ranked_Down)
as_callback('rated7x7_Down', rated7x7_Down)
as_callback('team7x7_Down', team7x7_Down)
as_callback('total_Down', total_Down)
as_callback('minimize_Down', minimize_Down)


def clan_Over(data):
    global selectField
    selectField = 'Clan'
    as_event('ON_CLAN')


def company_Over(data):
    global selectField
    selectField = 'Company'
    as_event('ON_COMPANY')


def epicRandomn_Over(data):
    global selectField
    selectField = 'EpicRandom'
    as_event('ON_EPIC_RANDOM')


def fallout_Over(data):
    global selectField
    selectField = 'Fallout'
    as_event('ON_FALLOUT')


def fortBattles_Over(data):
    global selectField
    selectField = 'FortBattles'
    as_event('ON_FORT_BATTLES')


def fortSorties_Over(data):
    global selectField
    selectField = 'FortSorties'
    as_event('ON_FORT_SORTIES')


def globalMap_Over(data):
    global selectField
    selectField = 'GlobalMap'
    as_event('ON_GLOBAL_MAP')


def random_Over(data):
    global selectField
    selectField = 'Random'
    as_event('ON_RANDOM')


def ranked_Over(data):
    global selectField
    selectField = 'Ranked'
    as_event('ON_RANKED')


def rated7x7_Over(data):
    global selectField
    selectField = 'Rated7x7'
    as_event('ON_RATED7X7')


def team7x7_Over(data):
    global selectField
    selectField = 'Team7x7'
    as_event('ON_TEAM7X7')


def total_Over(data):
    global selectField
    selectField = 'Total'
    as_event('ON_TOTAL')


def minimize_Over(data):
    global selectField
    selectField = 'Minimize'
    as_event('ON_MINIMIZE')


as_callback('clan_Over', clan_Over)
as_callback('company_Over', company_Over)
as_callback('epicRandomn_Over', epicRandomn_Over)
as_callback('fallout_Over', fallout_Over)
as_callback('fortBattles_Over', fortBattles_Over)
as_callback('fortSorties_Over', fortSorties_Over)
as_callback('globalMap_Over', globalMap_Over)
as_callback('random_Over', random_Over)
as_callback('ranked_Over', ranked_Over)
as_callback('rated7x7_Over', rated7x7_Over)
as_callback('team7x7_Over', team7x7_Over)
as_callback('total_Over', total_Over)
as_callback('minimize_Over', minimize_Over)


def clan_Out(data):
    global selectField
    if selectField == 'Clan':
        selectField = None
        as_event('ON_CLAN')


def company_Out(data):
    global selectField
    if selectField == 'Company':
        selectField = None
        as_event('ON_COMPANY')


def epicRandomn_Out(data):
    global selectField
    if selectField == 'EpicRandom':
        selectField = None
        as_event('ON_EPIC_RANDOM')


def fallout_Out(data):
    global selectField
    if selectField == 'Fallout':
        selectField = None
        as_event('ON_FALLOUT')


def fortBattles_Out(data):
    global selectField
    if selectField == 'FortBattles':
        selectField = None
        as_event('ON_FORT_BATTLES')


def fortSorties_Out(data):
    global selectField
    if selectField == 'FortSorties':
        selectField = None
        as_event('ON_FORT_SORTIES')


def globalMap_Out(data):
    global selectField
    if selectField == 'GlobalMap':
        selectField = None
        as_event('ON_GLOBAL_MAP')


def random_Out(data):
    global selectField
    if selectField == 'Random':
        selectField = None
        as_event('ON_RANDOM')


def ranked_Out(data):
    global selectField
    if selectField == 'Ranked':
        selectField = None
        as_event('ON_RANKED')


def rated7x7_Out(data):
    global selectField
    if selectField == 'Rated7x7':
        selectField = None
        as_event('ON_RATED7X7')


def team7x7_Out(data):
    global selectField
    if selectField == 'Team7x7':
        selectField = None
        as_event('ON_TEAM7X7')


def total_Out(data):
    global selectField
    if selectField == 'Total':
        selectField = None
        as_event('ON_TOTAL')


def minimize_Out(data):
    global selectField
    if selectField == 'Minimize':
        selectField = None
        as_event('ON_MINIMIZE')


as_callback('clan_Out', clan_Out)
as_callback('company_Out', company_Out)
as_callback('epicRandomn_Out', epicRandomn_Out)
as_callback('fallout_Out', fallout_Out)
as_callback('fortBattles_Out', fortBattles_Out)
as_callback('fortSorties_Out', fortSorties_Out)
as_callback('globalMap_Out', globalMap_Out)
as_callback('random_Out', random_Out)
as_callback('ranked_Out', ranked_Out)
as_callback('rated7x7_Out', rated7x7_Out)
as_callback('team7x7_Out', team7x7_Out)
as_callback('total_Out', total_Out)
as_callback('minimize_Out', minimize_Out)


@registerEvent(g_currentVehicle, '_selectVehicle')
def g_currentVehicle_selectVehicle(*args, **kwargs):
    global currentVehicle
    if g_currentVehicle.item is not None:
        # log('items = %s' % (filter(lambda x: not x.startswith('__'), dir(g_currentVehicle.item))))
        # log('Vehicle = %s' % g_currentVehicle.item.userName)
        dossier = g_currentVehicle.getDossier()
        currentVehicle['Clan'] = dossier.getClanStats()
        currentVehicle['Company'] = dossier.getCompanyStats()
        currentVehicle['EpicRandom'] = dossier.getEpicRandomStats()
        currentVehicle['Fallout'] = dossier.getFalloutStats()
        currentVehicle['FortBattles'] = dossier.getFortBattlesStats()
        currentVehicle['FortSorties'] = dossier.getFortSortiesStats()
        currentVehicle['GlobalMap'] = dossier.getGlobalMapStats()
        currentVehicle['Random'] = dossier.getRandomStats()
        currentVehicle['Ranked'] = dossier.getRankedStats()
        currentVehicle['Rated7x7'] = dossier.getRated7x7Stats()
        currentVehicle['Team7x7'] = dossier.getTeam7x7Stats()
        currentVehicle['Total'] = dossier.getTotalStats()
        as_event(events[visibleStats])
        as_event('ON_SELECTED_VEHICLE')


@xvm.export('vs.highlight', deterministic=False)
def vs_highlight(typeStats):
    return 'highlight' if selectField == typeStats else None


@xvm.export('vs.selected', deterministic=False)
def vs_selected(typeStats):
    return 'selected' if visibleStats == typeStats else None

# export statistics macros

@xvm.export('vs.armorEfficiency', deterministic=False)
def vs_getArmorUsingEfficiency(typeStats):
    return currentVehicle[typeStats].getArmorUsingEfficiency() if typeStats in currentVehicle else None


@xvm.export('vs.avgDamage', deterministic=False)
def vs_getAvgDamage(typeStats):
    return currentVehicle[typeStats].getAvgDamage() if typeStats in currentVehicle else None


@xvm.export('vs.avgDamageAssistedStun', deterministic=False)
def vs_getAvgDamageAssistedStun(typeStats):
    return currentVehicle[typeStats].getAvgDamageAssistedStun() if typeStats in currentVehicle else None


@xvm.export('vs.avgDamageBlocked', deterministic=False)
def vs_getAvgDamageBlocked(typeStats):
    return currentVehicle[typeStats].getAvgDamageBlocked() if typeStats in currentVehicle else None


@xvm.export('vs.avgDamageReceived', deterministic=False)
def vs_getAvgDamageReceived(typeStats):
    return currentVehicle[typeStats].getAvgDamageReceived() if typeStats in currentVehicle else None


@xvm.export('vs.avgEnemiesSpotted', deterministic=False)
def vs_getAvgEnemiesSpotted(typeStats):
    return currentVehicle[typeStats].getAvgEnemiesSpotted() if typeStats in currentVehicle else None


@xvm.export('vs.avgFrags', deterministic=False)
def vs_getAvgFrags(typeStats):
    return currentVehicle[typeStats].getAvgFrags() if typeStats in currentVehicle else None


@xvm.export('vs.avgStunNumber', deterministic=False)
def vs_getAvgStunNumber(typeStats):
    return currentVehicle[typeStats].getAvgStunNumber() if typeStats in currentVehicle else None


@xvm.export('vs.avgXP', deterministic=False)
def vs_getAvgXP(typeStats):
    return currentVehicle[typeStats].getAvgXP() if typeStats in currentVehicle else None


@xvm.export('vs.battlesCount', deterministic=False)
def vs_getBattlesCount(typeStats):
    return currentVehicle[typeStats].getBattlesCount() if typeStats in currentVehicle else None


@xvm.export('vs.battlesCountVer2', deterministic=False)
def vs_getBattlesCountVer2(typeStats):
    return currentVehicle[typeStats].getBattlesCountVer2() if typeStats in currentVehicle else None


@xvm.export('vs.battlesCountVer3', deterministic=False)
def vs_getBattlesCountVer3(typeStats):
    return currentVehicle[typeStats].getBattlesCountVer3() if typeStats in currentVehicle else None


@xvm.export('vs.battlesCountWithStun', deterministic=False)
def vs_getBattlesCountWithStun(typeStats):
    return currentVehicle[typeStats].getBattlesCountWithStun() if typeStats in currentVehicle else None


@xvm.export('vs.capturePoints', deterministic=False)
def vs_getCapturePoints(typeStats):
    return currentVehicle[typeStats].getCapturePoints() if typeStats in currentVehicle else None


@xvm.export('vs.damageAssistedEfficiency', deterministic=False)
def vs_getDamageAssistedEfficiency(typeStats):
    return currentVehicle[typeStats].getDamageAssistedEfficiency() if typeStats in currentVehicle else None


@xvm.export('vs.damageAssistedRadio', deterministic=False)
def vs_getDamageAssistedRadio(typeStats):
    return currentVehicle[typeStats].getDamageAssistedRadio() if typeStats in currentVehicle else None


@xvm.export('vs.damageAssistedTrack', deterministic=False)
def vs_getDamageAssistedTrack(typeStats):
    return currentVehicle[typeStats].getDamageAssistedTrack() if typeStats in currentVehicle else None


@xvm.export('vs.damageBlockedByArmor', deterministic=False)
def vs_getDamageBlockedByArmor(typeStats):
    return currentVehicle[typeStats].getDamageBlockedByArmor() if typeStats in currentVehicle else None


@xvm.export('vs.damageDealt', deterministic=False)
def vs_getDamageDealt(typeStats):
    return currentVehicle[typeStats].getDamageDealt() if typeStats in currentVehicle else None


@xvm.export('vs.damageEfficiency', deterministic=False)
def vs_getDamageEfficiency(typeStats):
    return currentVehicle[typeStats].getDamageEfficiency() if typeStats in currentVehicle else None


@xvm.export('vs.damageReceived', deterministic=False)
def vs_getDamageReceived(typeStats):
    return currentVehicle[typeStats].getDamageReceived() if typeStats in currentVehicle else None


@xvm.export('vs.deathsCount', deterministic=False)
def vs_getDeathsCount(typeStats):
    return currentVehicle[typeStats].getDeathsCount() if typeStats in currentVehicle else None


@xvm.export('vs.drawsCount', deterministic=False)
def vs_getDrawsCount(typeStats):
    return currentVehicle[typeStats].getDrawsCount() if typeStats in currentVehicle else None


@xvm.export('vs.droppedCapturePoints', deterministic=False)
def vs_getDroppedCapturePoints(typeStats):
    return currentVehicle[typeStats].getDroppedCapturePoints() if typeStats in currentVehicle else None


@xvm.export('vs.frags8p', deterministic=False)
def vs_getFrags8p(typeStats):
    return currentVehicle[typeStats].getFrags8p() if typeStats in currentVehicle else None


@xvm.export('vs.fragsCount', deterministic=False)
def vs_getFragsCount(typeStats):
    return currentVehicle[typeStats].getFragsCount() if typeStats in currentVehicle else None


@xvm.export('vs.fragsEfficiency', deterministic=False)
def vs_getFragsEfficiency(typeStats):
    return currentVehicle[typeStats].getFragsEfficiency() if typeStats in currentVehicle else None


@xvm.export('vs.heHits', deterministic=False)
def vs_getHeHits(typeStats):
    return currentVehicle[typeStats].getHeHits() if typeStats in currentVehicle else None


@xvm.export('vs.heHitsReceived', deterministic=False)
def vs_getHeHitsReceived(typeStats):
    return currentVehicle[typeStats].getHeHitsReceived() if typeStats in currentVehicle else None


@xvm.export('vs.hitsCount', deterministic=False)
def vs_getHitsCount(typeStats):
    return currentVehicle[typeStats].getHitsCount() if typeStats in currentVehicle else None


@xvm.export('vs.hitsEfficiency', deterministic=False)
def vs_getHitsEfficiency(typeStats):
    return currentVehicle[typeStats].getHitsEfficiency() if typeStats in currentVehicle else None


@xvm.export('vs.lossesCount', deterministic=False)
def vs_getLossesCount(typeStats):
    return currentVehicle[typeStats].getLossesCount() if typeStats in currentVehicle else None


@xvm.export('vs.lossesEfficiency', deterministic=False)
def vs_getLossesEfficiency(typeStats):
    return currentVehicle[typeStats].getLossesEfficiency() if typeStats in currentVehicle else None


@xvm.export('vs.noDamageShotsReceived', deterministic=False)
def vs_getNoDamageShotsReceived(typeStats):
    return currentVehicle[typeStats].getNoDamageShotsReceived() if typeStats in currentVehicle else None


@xvm.export('vs.originalXP', deterministic=False)
def vs_getOriginalXP(typeStats):
    return currentVehicle[typeStats].getOriginalXP() if typeStats in currentVehicle else None


@xvm.export('vs.pierced', deterministic=False)
def vs_getPierced(typeStats):
    return currentVehicle[typeStats].getPierced() if typeStats in currentVehicle else None


@xvm.export('vs.piercedReceived', deterministic=False)
def vs_getPiercedReceived(typeStats):
    return currentVehicle[typeStats].getPiercedReceived() if typeStats in currentVehicle else None


@xvm.export('vs.potentialDamageReceived', deterministic=False)
def vs_getPotentialDamageReceived(typeStats):
    return currentVehicle[typeStats].getPotentialDamageReceived() if typeStats in currentVehicle else None


@xvm.export('vs.record', deterministic=False)
def vs_getRecord(typeStats):
    return currentVehicle[typeStats].getRecord() if typeStats in currentVehicle else None


@xvm.export('vs.shotsCount', deterministic=False)
def vs_getShotsCount(typeStats):
    return currentVehicle[typeStats].getShotsCount() if typeStats in currentVehicle else None


@xvm.export('vs.shotsReceived', deterministic=False)
def vs_getShotsReceived(typeStats):
    return currentVehicle[typeStats].getShotsReceived() if typeStats in currentVehicle else None


@xvm.export('vs.spottedEnemiesCount', deterministic=False)
def vs_getSpottedEnemiesCount(typeStats):
    return currentVehicle[typeStats].getSpottedEnemiesCount() if typeStats in currentVehicle else None


@xvm.export('vs.stunNumber', deterministic=False)
def vs_getStunNumber(typeStats):
    return currentVehicle[typeStats].getStunNumber() if typeStats in currentVehicle else None


@xvm.export('vs.survivalEfficiency', deterministic=False)
def vs_getSurvivalEfficiency(typeStats):
    return currentVehicle[typeStats].getSurvivalEfficiency() if typeStats in currentVehicle else None


@xvm.export('vs.survivedBattlesCount', deterministic=False)
def vs_getSurvivedBattlesCount(typeStats):
    return currentVehicle[typeStats].getSurvivedBattlesCount() if typeStats in currentVehicle else None


@xvm.export('vs.winAndSurvived', deterministic=False)
def vs_getWinAndSurvived(typeStats):
    return currentVehicle[typeStats].getWinAndSurvived() if typeStats in currentVehicle else None


@xvm.export('vs.winsCount', deterministic=False)
def vs_getWinsCount(typeStats):
    return currentVehicle[typeStats].getWinsCount() if typeStats in currentVehicle else None


@xvm.export('vs.winsEfficiency', deterministic=False)
def vs_getWinsEfficiency(typeStats):
    winsEfficiency = currentVehicle[typeStats].getWinsEfficiency()
    return winsEfficiency * 100 if (typeStats in currentVehicle) and (winsEfficiency is not None) else None


@xvm.export('vs.XP', deterministic=False)
def vs_getXP(typeStats):
    return currentVehicle[typeStats].getXP() if typeStats in currentVehicle else None
